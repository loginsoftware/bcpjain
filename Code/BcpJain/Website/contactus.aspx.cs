﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contactus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnSend_Click(object sender, EventArgs e)
    {
        Utility Ut = new Utility();
        string _name = Request.Form["name"].ToString();
        string _email = Request.Form["email"].ToString();
        string _phone = Request.Form["phone"].ToString();
        string _subject = Request.Form["subject"].ToString();
        string _message = Request.Form["message"].ToString();

        string _body = "<table>"
                     + "     <tr>"
                     + "         <td>Name:</td>"
                     + "         <td>" + _name + "</td>"
                     + "     </tr>"
                     + "     <tr>"
                     + "         <td>Emailid:</td>"
                     + "         <td>" + _email + "</td>"
                     + "     </tr>"
                     + "     <tr>"
                     + "         <td>MobileNo:</td>"
                     + "         <td>" + _phone + "</td>"
                     + "     </tr>"
                     + "     <tr>"
                     + "         <td>Subject:</td>"
                     + "         <td>" + _subject + "</td>"
                     + "     </tr>"
                     + "     <tr>"
                     + "         <td>Message:</td>"
                     + "         <td>" + _message + "</td>"
                     + "     </tr>"
                     + " </table>";

        Thread email = new Thread(delegate()
        {
          // Ut.SendMail("pradeeplodhivds@gmail.com", "bcpjain.com New Enquiry Recieved " + DateTime.Now, _body);
            Ut.SendMail("bcpjainwebsite@gmail.com", "bcpjain.com New Enquiry Recieved" + DateTime.Now, _body);

        });

        email.IsBackground = true;
        email.Start();

        Ut.ShowMessage("Message Sent Successfully !", this.Page);

    }
}