﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="news-and-updates.aspx.cs" Inherits="news_and_updates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="wrapper_main about_us_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							News & Updates
						</h3> </div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="about_left_part">
						<asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="NewsUpdateId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
      <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>                                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="File Download">
                            <ItemTemplate>
                              <a target="_blank" href='<%# "../" + Eval("FilePath") %>'>Download</a>
                                <asp:HiddenField ID="hfFilePath" runat="server" Value='<%# Eval("FilePath") %>' />       
                            </ItemTemplate>
                            </asp:TemplateField>    
                                                        
                            </Columns>
                            </asp:GridView>
                           
                           	</div>
				</div>
				
			</div>
		</div>
	</div>
</asp:Content>

