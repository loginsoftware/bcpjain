﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ClientMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["clientId"] != null as string)
            {
                string clientId = Request.QueryString["clientId"].ToString();
                LoadClientDetailById(clientId);
            }
        }
    }

    private void LoadClientDetailById(string clientId)
    {
        mySql objsql = new mySql();
        DataTable Dt = objsql.LoadAllClientByClientId(int.Parse(clientId));
        hfPKey.Value = Dt.Rows[0]["ClientId"].ToString();
        TxtName.Text = Dt.Rows[0]["FullName"].ToString();
        TxtDesignation.Text = Dt.Rows[0]["Designation"].ToString();
        TxtMobileNo.Text = Dt.Rows[0]["MobileNo"].ToString();
        TxtEmail.Text = Dt.Rows[0]["EmailId"].ToString();
        TxtAddress.Text = Dt.Rows[0]["Address"].ToString();
        
        TxtUserName.Text = Dt.Rows[0]["UserName"].ToString();
        TxtPassword.Text = Dt.Rows[0]["Password"].ToString();
        hfUserId.Value = Dt.Rows[0]["UserId"].ToString();
        BtnSave.Text = "Update";

    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        mySql objsql = new mySql();
        Utility Ut = new Utility();
        if (BtnSave.Text == "Save")
        {
            int _userId = objsql.InsertLoginDetail(TxtName.Text, TxtUserName.Text, TxtPassword.Text, "Client");
            if (_userId == 0)
            {
                Ut.ShowMessage("UserName already Exist !", this.Page);
            }
            else
            {
                objsql.InsertClientMaster(TxtName.Text, TxtDesignation.Text, TxtEmail.Text, TxtMobileNo.Text, TxtAddress.Text, DateTime.Now, _userId);
                Ut.ShowMessage("Client added Successfully !", this.Page);
            }
        }
        else if (BtnSave.Text == "Update")
        {
            int _userId = objsql.InsertLoginDetailUpdate(int.Parse(hfUserId.Value), TxtName.Text, TxtUserName.Text, TxtPassword.Text, "Client");
            if (_userId == 0)
            {
                Ut.ShowMessage("UserName already Exist !", this.Page);
            }
            else
            {
                objsql.InsertClientMasterUpdate(int.Parse(hfPKey.Value), TxtName.Text, TxtDesignation.Text, TxtEmail.Text, TxtMobileNo.Text, TxtAddress.Text, DateTime.Now, _userId);
                Ut.ShowMessage("Client Updated Successfully !", this.Page);
            }
        }
    }
}