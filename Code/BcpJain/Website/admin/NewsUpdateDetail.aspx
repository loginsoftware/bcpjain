﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="NewsUpdateDetail.aspx.cs" Inherits="admin_NewsUpdateDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Client Detail</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="NewsUpdateId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="CreatedOn">
                            <ItemTemplate>
      <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>                                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="File">
                            <ItemTemplate>
                              <a href='<%#  "../" + Eval("FilePath") %>'>Download</a>
                                <asp:HiddenField ID="hfFilePath" runat="server" Value='<%#  "~/" + Eval("FilePath") %>' />       
                            </ItemTemplate>
                            </asp:TemplateField>    
                             
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-danger" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Delete">Delete</asp:LinkButton> 
                            
                           
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

