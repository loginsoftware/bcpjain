﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RecievedFileFromAdminToClient : System.Web.UI.Page
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadClient();
        }
        
    }

    private void LoadClient()
    {
        int _userId = int.Parse(Ut.GetCookie(this.Page, "UserId"));
        mySql objsql = new mySql();
        GVDetail.DataSource = objsql.LoadTaskBySentToUserId(_userId);
        GVDetail.DataBind();
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("FileSubmitedByEmployee.aspx?TaskId=" + Pkey);
    }
}