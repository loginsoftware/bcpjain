﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_NewsUpdateDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadNewsUpdate();
        }
    }

    private void LoadNewsUpdate()
    {
        mySql objsql = new mySql();
        GVDetail.DataSource = objsql.LoadAllNewsUpdates();
        GVDetail.DataBind();
    }
    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();

        mySql objsql = new mySql();

        HiddenField hfFilePath = (HiddenField)GVDetail.Rows[gvRow.RowIndex].FindControl("hfFilePath");

        string Deletefile = Server.MapPath("~/" + hfFilePath.Value);
        FileInfo file = new FileInfo(Deletefile);
        if (file.Exists)
        {
            file.Delete();
        }

        int _result = objsql.DeleteNewsUpdates(int.Parse(Pkey));
        if (_result != 0)
        {
            Utility Ut = new Utility();
            Ut.ShowMessage("Delete Success !", this.Page);
        }
        LoadNewsUpdate();
        
    }
}