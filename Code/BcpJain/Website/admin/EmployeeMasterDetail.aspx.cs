﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_EmployeeMasterDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadEmployee();
        }
    }

    private void LoadEmployee()
    {
        mySql objsql = new mySql();
        GVDetail.DataSource = objsql.LoadAllEmployeeDetail();
        GVDetail.DataBind();
    }
    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("EmployeeMaster.aspx?empId=" + Pkey);
    }
}