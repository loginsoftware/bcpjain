﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="RecievedFileFromAdminToClient.aspx.cs" Inherits="admin_RecievedFileFromAdminToClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
     <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-user"></i> Recieved Files From Admin</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                                  <asp:GridView ID="GVDetail" AutoGenerateColumns="False" AllowPaging="false" 
            runat="server" DataKeyNames="TaskId" EmptyDataText="No Record Found" 
            Width="100%" CssClass="table table-striped table-bordered responsive">
                            <AlternatingRowStyle BackColor="#ecebeb" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                                 <asp:TemplateField HeaderText="CreatedOn">
                            <ItemTemplate>
      <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Client">
                            <ItemTemplate>
                                <asp:Label ID="lblClientName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>  
                                ( <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>   )                        
                            </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblemail" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                                      
                            </ItemTemplate>
                            </asp:TemplateField>                                
                        <asp:TemplateField HeaderText="Mobile Number">
                            <ItemTemplate>
                             <asp:Label ID="lblmobile" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>   
                                 <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                             <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField> 
                             <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
               <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' CssClass='<%# Eval("Status").ToString().Contains("New") ? "label label-danger" : Eval("Status").ToString().Contains("Assigned") ? "label label-warning" : "label label-success" %>'></asp:Label>
 <br />   <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#  "~/" + Eval("FilePath") %>' Target="_blank">Download File</asp:HyperLink>
                            </ItemTemplate>
                            </asp:TemplateField>  
                                                               
    <asp:TemplateField HeaderText="Action">
                      <ItemTemplate>
                   <asp:LinkButton ID="LnkBtnEdit" CssClass="btn btn-success" runat="server" onclick="LnkBtnEdit_Click"
                            ToolTip="Click here to Update"  Visible='<%# Eval("Status").ToString().Contains("Work Done") ? false : true %>'>View & Update</asp:LinkButton>    
                      </ItemTemplate>
                      </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>


                    
                </div>
            </div>
        </div>
        <!--/span-->

    </div>
</asp:Content>

