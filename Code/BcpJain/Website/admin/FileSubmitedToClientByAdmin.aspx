﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="FileSubmitedToClientByAdmin.aspx.cs" Inherits="admin_FileSubmitedToClientByAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> View & Update File to Admin</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
           <div class="box-content">
                <div role="form">
                    <div class="form-group">
                        <label>Client Name</label>
  <asp:Label ID="lblClientName" runat="server" Text=""></asp:Label><br />
   <label>Mobile No: </label><asp:Label ID="lblClientMobile" runat="server" Text=""></asp:Label><br />
  <label>Email: </label> <asp:Label ID="lblClientEmail" runat="server" Text=""></asp:Label><br />
   <label>Address: </label><asp:Label ID="lblClientAddress" runat="server" Text=""></asp:Label><br />



                    </div>
                    <div class="form-group">
                        <label>Browse File</label>
    <asp:FileUpload ID="FUFile" runat="server" />
   <asp:HyperLink ID="lnkFileUrl" runat="server" Target="_blank">Download File</asp:HyperLink>
                         <span style="color:red;">file type: xlsx,pdf,docx,png,jpg,jpeg only</span>
                    </div>
                     <div class="form-group">
                        <label>Remark/Description/Message</label>
    <asp:TextBox ID="TxtDescription" runat="server" CssClass="form-control" placeholder="Enter Description" TextMode="MultiLine"></asp:TextBox>
                    </div>
                 
    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
   <asp:HiddenField ID="hfFilePath" runat="server" />
        <asp:HiddenField ID="hfClientUserId" runat="server" />
        <asp:HiddenField ID="hfTaskId" runat="server" />
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>
</asp:Content>

