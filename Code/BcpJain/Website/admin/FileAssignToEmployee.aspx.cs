﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_FileAssignToEmployee : System.Web.UI.Page
{
    mySql objsql = new mySql();
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["TaskId"] != null as string)
            {
                string _taskId = Request.QueryString["TaskId"].ToString();
                LoadTaskMasterByTaskId(int.Parse(_taskId));
                hfTaskId.Value = _taskId;
            }
        }
        
    }

    private void LoadTaskMasterByTaskId(int taskId)
    
    {
        DataTable Dt = objsql.LoadTaskByTaskId(taskId);
        TxtDescription.Text = Dt.Rows[0]["Description"].ToString();
        lnkFileUrl.NavigateUrl = Dt.Rows[0]["FilePath"].ToString();
        hfFilePath.Value = Dt.Rows[0]["FilePath"].ToString();
        try
        {
            DdnEmployee.SelectedValue = Dt.Rows[0]["SentToUserId"].ToString();
        }
        catch { }
       
        string _clientUserId = Dt.Rows[0]["CreatedByUserId"].ToString();

        DataTable Dt2 = objsql.LoadClientByUserId(int.Parse(_clientUserId));
        lblClientName.Text = Dt2.Rows[0]["FullName"].ToString();
        lblClientMobile.Text = Dt2.Rows[0]["MobileNo"].ToString();
        lblClientEmail.Text = Dt2.Rows[0]["EmailId"].ToString();
        lblClientAddress.Text = Dt2.Rows[0]["Address"].ToString();
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        int _userId = int.Parse(Ut.GetCookie(this.Page, "UserId"));
        int _result = objsql.UpdateTaskAssignedToEmployee(int.Parse(hfTaskId.Value), TxtDescription.Text, hfFilePath.Value, _userId, int.Parse(DdnEmployee.SelectedValue), "Assigned to Employee " + DdnEmployee.SelectedItem.Text);
        if (_result != 0)
        {
            Ut.ShowMessage("File Assigned to Employee.", this.Page);
        }
    }
}