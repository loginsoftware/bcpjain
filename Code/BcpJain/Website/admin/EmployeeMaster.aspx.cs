﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_EmployeeMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["empId"] != null as string)
            {
                string _empId = Request.QueryString["empId"].ToString();
                LoadEmpDetailById(_empId);
            }
        }
    }

    private void LoadEmpDetailById(string _empId)
    {
        mySql objsql = new mySql();
        DataTable Dt = objsql.LoadAllEmployeByEmpId(int.Parse(_empId));
        hfPKey.Value = Dt.Rows[0]["EmployeeId"].ToString();
        TxtName.Text = Dt.Rows[0]["EmployeeName"].ToString();
        TxtDesignation.Text = Dt.Rows[0]["Designation"].ToString();
        TxtMobileNo.Text = Dt.Rows[0]["MobileNo"].ToString();
        TxtEmail.Text = Dt.Rows[0]["EmailId"].ToString();
        TxtAddress.Text = Dt.Rows[0]["Address"].ToString();
        DdnUserType.SelectedValue = Dt.Rows[0]["UserType"].ToString();
        TxtUserName.Text = Dt.Rows[0]["UserName"].ToString();
        TxtPassword.Text = Dt.Rows[0]["Password"].ToString();
        hfUserId.Value = Dt.Rows[0]["UserId"].ToString();
        BtnSave.Text = "Update";

    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        mySql objsql = new mySql();
        Utility Ut = new Utility();
        if (BtnSave.Text == "Save")
        {
            int _userId = objsql.InsertLoginDetail(TxtName.Text, TxtUserName.Text, TxtPassword.Text, DdnUserType.SelectedItem.Text);
            if (_userId == 0)
            {
                Ut.ShowMessage("UserName already Exist !", this.Page);
            }
            else
            {
                objsql.InsertEmpMaster(TxtName.Text, TxtDesignation.Text, TxtEmail.Text, TxtMobileNo.Text, TxtAddress.Text, DateTime.Now, _userId);
                Ut.ShowMessage("Employee added Successfully !", this.Page);
            }
        }
        else if (BtnSave.Text == "Update")
        {
            int _userId = objsql.InsertLoginDetailUpdate(int.Parse(hfUserId.Value), TxtName.Text, TxtUserName.Text, TxtPassword.Text, DdnUserType.SelectedItem.Text);
            if (_userId == 0)
            {
                Ut.ShowMessage("UserName already Exist !", this.Page);
            }
            else
            {
                objsql.InsertEmpMasterUpdate(int.Parse(hfPKey.Value), TxtName.Text, TxtDesignation.Text, TxtEmail.Text, TxtMobileNo.Text, TxtAddress.Text, DateTime.Now, _userId);
                Ut.ShowMessage("Employee Updated Successfully !", this.Page);
            }
        }
    }
}