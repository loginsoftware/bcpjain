﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PriyalAccountant_Default : System.Web.UI.Page
{
    Utility Ut = new Utility();
    mySql objsql = new mySql();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //AndroidGCMPushNotification push = new AndroidGCMPushNotification();
            //string _result = push.SendGCMNotification("cssrZUc3rQo:APA91bHI8nyN0uB96CrZSOmrO7qDYDTP8WMFb3o9P5WYvaPUDkvrMl2vSzCA4CKx1J_8r9XJ9HiPf86ZSlFJhXKGXqK_Gee3JRSFDLfTCsbYRoFcFQWZAt3gYh34ECMn5sr-iMe_rZ5V", "Visit Alert", "you have New Visit click here to check");
            //Response.Write(_result);
            //Ut.SendSmsNewVisit("9993184672", "Pooja Lodhi", "8982314615", "Relation");
        }
    }
    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        DataTable Dt = objsql.LoginUser(txtUserName.Value);
        string _password = Dt.Rows[0]["Password"].ToString();
        string _userType = Dt.Rows[0]["UserType"].ToString();
        string _UserId = Dt.Rows[0]["UserId"].ToString();
        string _FullName = Dt.Rows[0]["FullName"].ToString();
        if (_password == txtPassword.Value)
        {
            Ut.SetCookie(this.Page, "UserId", _UserId);
            Ut.SetCookie(this.Page, "FullName", _FullName);
            Ut.SetCookie(this.Page, "UserType", _userType);
            Response.Redirect("Dashboard.aspx");
        }
        else
        {
            Ut.ShowMessage("Invalid Username and Password !", this.Page);
        }

       
       
    }
       
}