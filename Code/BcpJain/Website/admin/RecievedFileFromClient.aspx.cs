﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RecievedFileFromClient : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadClient();
        }
        
    }

    private void LoadClient()
    {
        mySql objsql = new mySql();
        GVDetail.DataSource = objsql.LoadAllTaskFromClient();
        GVDetail.DataBind();
    }

    protected void LnkBtnEdit_Click(object sender, EventArgs e)
    {
        LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("FileAssignToEmployee.aspx?TaskId=" + Pkey);
    }
    protected void LnkBtnSendClient_Click(object sender, EventArgs e)
    {
         LinkButton LnkBtn = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)LnkBtn.NamingContainer;
        string Pkey = GVDetail.DataKeys[gvRow.RowIndex].Values[0].ToString();
        Response.Redirect("FileSubmitedToClientByAdmin.aspx?TaskId=" + Pkey);
        
    }
}