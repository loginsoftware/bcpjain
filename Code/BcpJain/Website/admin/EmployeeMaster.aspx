﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.master" AutoEventWireup="true" CodeFile="EmployeeMaster.aspx.cs" Inherits="admin_EmployeeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i>Employee Master</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div role="form">
                     <div class="form-group">
                        <label>Name</label>
    <asp:TextBox ID="TxtName" runat="server" CssClass="form-control" placeholder="Enter Name"></asp:TextBox>
                    </div>
                <div class="form-group">
                        <label>Designation</label>
    <asp:TextBox ID="TxtDesignation" runat="server" CssClass="form-control" placeholder="Enter Designation"></asp:TextBox>
                    </div>
                   
                     <div class="form-group">
                        <label>Mobile Number</label>
    <asp:TextBox ID="TxtMobileNo" runat="server" CssClass="form-control" placeholder="Enter Mobile Number"></asp:TextBox>
                    </div>
                   <div class="form-group">
                        <label>EmailId</label>
    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control" placeholder="Enter Email Id"></asp:TextBox>
                    </div>
                     <div class="form-group">
                        <label>Address</label>
    <asp:TextBox ID="TxtAddress" runat="server" CssClass="form-control" placeholder="Enter Address"></asp:TextBox>
                    </div>
                     <div class="form-group">
                        <label>UserRole</label>
    <asp:DropDownList ID="DdnUserType" runat="server" Width="100%" Height="32px">
    <asp:ListItem>--Select--</asp:ListItem>
    <asp:ListItem>Employee</asp:ListItem>
    <asp:ListItem>Admin</asp:ListItem>
    </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>UserName</label>
    <asp:TextBox ID="TxtUserName" runat="server" CssClass="form-control" placeholder="Enter UserName"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
    <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" placeholder="Enter Password"></asp:TextBox>
                    </div>
    <asp:LinkButton ID="BtnSave" runat="server" CssClass="btn btn-default" OnClick="BtnSave_Click">Save</asp:LinkButton>
    <asp:HiddenField ID="hfUserId" runat="server" />
    <asp:HiddenField ID="hfPKey" runat="server" />
                </div>

            </div>
        </div>
    </div>
    <!--/span-->

</div>
</asp:Content>

