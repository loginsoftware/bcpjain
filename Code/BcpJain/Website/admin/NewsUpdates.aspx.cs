﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_NewsUpdates : System.Web.UI.Page
{

    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!chkAll()) return;

        mySql objsql = new mySql ();
        SaveImage();
        int _result = objsql.InsertNewsUpdate(TxtDescription.Text, hfFilePath.Value);
        if (_result != 0)
        {
            Ut.ShowMessage("Record Saved Successfully !", this.Page);
        }
    }

    private bool chkAll()
    {

        if (TxtDescription.Text == "")
        {
            Ut.ShowMessage("Enter Description or Message", this.Page);
            return false;
        }

        if (!FUFile.HasFile)
        {
            Ut.ShowMessage("Please Select any file !", this.Page);
            return false;
        }

        return true;
    }

    private void SaveImage()
    {
        if (FUFile.HasFile)
        {
            string FileName = Path.GetFileName(FUFile.PostedFile.FileName);
            string FileExt = Path.GetExtension(FUFile.PostedFile.FileName);

            if (FileExt == ".psd" || FileExt == ".xlsx" || FileExt == ".xls" || FileExt == ".jpg" || FileExt == ".jpeg" || FileExt == ".png" || FileExt == ".pdf" || FileExt == ".docx")
            {
                FileName = Ut.CheckFileNameExist("ReadWrite", FileName);
                FUFile.SaveAs(Server.MapPath("~//ReadWrite//" + FileName));
                hfFilePath.Value = "ReadWrite/" + FileName;
            }
        }
        
    }
}