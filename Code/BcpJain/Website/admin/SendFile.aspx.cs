﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_SendFile : System.Web.UI.Page
{
    Utility Ut = new Utility();
    mySql objsql = new mySql();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (!chkAll()) return;
        int _adminUserId = 3;
        int _userId = int.Parse(Ut.GetCookie(this.Page, "UserId"));

        SaveImage();

        int _result = objsql.InsertTaskMaster(TxtDescription.Text, hfFilePath.Value, DateTime.Now, _userId, _adminUserId, false, "New", _userId);
        if (_result != 0)
        {
            Ut.ShowMessage("File Sent Successfully !", this.Page);
        }
    }

    private void SaveImage()
    {
        if (FUFile.HasFile)
        {
            string FileName = Path.GetFileName(FUFile.PostedFile.FileName);
            string FileExt = Path.GetExtension(FUFile.PostedFile.FileName);

            if (FileExt == ".xlsx" || FileExt == ".xls" || FileExt == ".jpg" || FileExt == ".jpeg" || FileExt == ".png" || FileExt == ".pdf" || FileExt == ".docx")
            {
                FileName = Ut.CheckFileNameExist("ReadWrite", FileName);
                FUFile.SaveAs(Server.MapPath("~//ReadWrite//" + FileName));
                hfFilePath.Value = "ReadWrite/" + FileName;
            }
        }

    }

    private bool chkAll()
    {
        if (TxtDescription.Text == "")
        {
            Ut.ShowMessage("Enter Some Remark/Message/Comment", this.Page);
            return false;
        }
        if (!FUFile.HasFile)
        {
            Ut.ShowMessage("Select any File to send !", this.Page);
            return false;
        }

        return true;
    }
}