﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin : System.Web.UI.MasterPage
{
    Utility Ut = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Ut.GetCookie(this.Page, "UserId") == null as string || Ut.GetCookie(this.Page, "UserId") == "")
            {
                Response.Redirect("Default.aspx", false);
            }
            else
            {
                string _userType = Ut.GetCookie(this.Page, "UserType");
                if (_userType == "Admin")
                {
                    ltrMenuAdmin.Visible = true;
                }
                else if (_userType == "Employee")
                {
                    LtrMenuEmployee.Visible = true;
                }
                else if (_userType == "Client")
                {
                    ltrMenuClient.Visible = true;
                }

            }
        }
    }
}
