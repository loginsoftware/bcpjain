﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <!--slider section Start-->
		<div class="wrapper_main slider_section_new">
			<!-- SLIDER -->
			<div class="example">
				<div class="content slider_version_3" style="width:100%; float:left;">
					<div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
						<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
						<div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
							<ul style="width:100% !important;">
								<!--slide-->
								<li data-index="rs-397" data-transition="parallaxhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide Mask" data-description="">
									<!-- MAIN IMAGE -->
									<img src="images/homeslider.png" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								
								</li>
								<!--slide-->
								<!-- SLIDE  -->
								
							</ul>
						</div>
					</div>
					<!-- END REVOLUTION SLIDER -->
				</div>
			</div>
			<!-- section end -->
			<!-- SLIDER -->
			
			<!--slider form section start-->
			<div class="wrapper_main slider_details_box_wrap">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-sm-7 col-xs-6 triangle_box">
							<div class="slider_details_box">
								<h1>CA BCP Jain & Co.</h1>
                                <p>B.C.P Jain & Co. is a prominent Chartered Accountancy firm in Bhopal, led by skilled and experienced Indian Chartered Accountants having industry experience of over a decade.</p>
								<a href="contactus.aspx"> contact us</a>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			<!--slider form section end-->
		</div>
    <!--History section start-->
	<div class="wrapper_main lw_history_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle title_home">
							Welcome to <span class="text-success"> BCP Jain & Company </span>
						</h3> </div>
				<div class="full_width">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="left_side_details">
							<h4 class="lw_left_title">
									we start since
									<span class="lw_year">
										1978
									</span>
								</h4>
							<p>M/s BCP Jain & CO. is a multi-location firm of Chartered Accountants Setup in 1978 situated at Bhopal, Indore, Raipur and Vidisha With a track record of over 40 years, we have been able to branch out to the fields of Audit, Corporate Law, Taxation (Direct and Indirect), Accounting, Management Consultancy, FCRA Consultancy, NGO's, Consultancy of Funds Management, Investment and Portfolio Management, BPO work and Consultancy on Costing Matters etc. </p>
							<div class="lw_button read_more"> <a href="aboutus.aspx">
										Read more
									</a> </div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="right_lawyer_img"> <img src="images/Chartered-Accountant.jpg" class="img-responsive" alt="bcpjain-aboutus"> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--History Section End-->

    <!--tab section start-->
	<div class="wrapper_main lw_practice_area_section">
		<div class="container">
			<div class="demo">
				<div class="tab tab-horiz">
					<div class="row">
						<div class="col-lg-12">
							<h3 class="lw_heading_middle title_home">
									Our Services
								</h3> </div>
						<!--top tab section start-->
						<ul class="tab-legend">
							
							<li class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-md-offset-3">
								<div class="hw_box_wrap box_wrap_main">
									<div class="hw_thumb"> 
										<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 51 63" enable-background="new 0 0 51 63" xml:space="preserve">
											<g id="_x39_">
												<path d="M47.666,8.574c-4.539-0.909-7.824-1.73-9.251-2.316c-1.499-0.614-4.908-2.511-9.599-5.342
												c-0.998-0.604-2.144-0.921-3.308-0.921c-1.172,0-2.32,0.321-3.323,0.929c-4.585,2.782-7.919,4.645-9.386,5.247
												C10.827,6.979,6.014,8.02,3.321,8.569c-1.736,0.354-2.975,1.956-2.881,3.728l1.359,25.325c0.336,6.54,3.585,12.478,8.916,16.285
												l10.668,7.625c1.208,0.863,2.631,1.316,4.117,1.316c1.485,0,2.908-0.453,4.117-1.316l10.67-7.625
												c5.33-3.808,8.577-9.745,8.915-16.284l1.357-25.317C50.655,10.528,49.411,8.924,47.666,8.574z M48.743,12.208l-1.359,25.318
												c-0.308,5.986-3.282,11.416-8.158,14.898l-10.667,7.624c-0.896,0.642-1.956,0.98-3.059,0.98c-1.104,0-2.16-0.339-3.058-0.98
												l-10.668-7.624c-4.877-3.482-7.849-8.912-8.157-14.9L2.26,12.199c-0.05-0.892,0.551-1.668,1.425-1.847
												c3.366-0.685,7.8-1.674,9.804-2.497c1.99-0.816,6.341-3.373,9.642-5.375c0.716-0.436,1.54-0.665,2.377-0.665
												c0.835,0,1.653,0.228,2.369,0.659c3.372,2.035,7.818,4.637,9.848,5.469c1.961,0.804,6.295,1.758,9.584,2.417
												C48.172,10.533,48.789,11.327,48.743,12.208z"/>
												<path d="M42.79,12.819c-3.391-0.678-5.923-1.309-6.95-1.73c-1.092-0.448-3.741-1.924-7.263-4.05
												c-0.928-0.56-1.989-0.855-3.071-0.855c-1.086,0-2.153,0.298-3.083,0.862c-3.443,2.089-6.03,3.538-7.098,3.976
												c-1.055,0.431-3.653,1.086-7.128,1.793c-1.823,0.372-3.124,2.054-3.023,3.913l1.047,19.526c0.276,5.351,2.935,10.208,7.294,13.322
												l8.227,5.88c1.103,0.788,2.403,1.203,3.759,1.203c1.356,0,2.657-0.415,3.758-1.203l8.227-5.88c4.362-3.114,7.021-7.97,7.295-13.318
												l1.048-19.523C45.927,14.869,44.621,13.186,42.79,12.819z M44.008,16.637L42.961,36.16c-0.246,4.794-2.628,9.144-6.533,11.934
												L28.2,53.973c-0.793,0.566-1.727,0.867-2.7,0.867c-0.975,0-1.908-0.301-2.7-0.867l-8.226-5.879
												c-3.907-2.79-6.289-7.14-6.535-11.935L6.992,16.63c-0.052-0.965,0.622-1.838,1.569-2.031c2.571-0.523,5.952-1.277,7.456-1.893
												c1.497-0.614,4.824-2.57,7.353-4.104c0.644-0.391,1.384-0.597,2.138-0.597c0.753,0,1.489,0.204,2.129,0.592
												c2.585,1.559,5.983,3.549,7.513,4.177c1.473,0.603,4.772,1.327,7.283,1.831C43.383,14.795,44.06,15.669,44.008,16.637z"/>
												<path d="M35.439,26.332h-6.437l-1.977-6.061c-0.217-0.665-0.831-1.113-1.531-1.113
												c-0.706,0-1.304,0.435-1.521,1.105l-1.977,6.068h-6.436c-0.695,0-1.308,0.45-1.525,1.119c-0.218,0.674,0.016,1.403,0.579,1.813
												l5.202,3.806l-1.986,6.111c-0.162,0.497-0.08,1.02,0.222,1.438c0.305,0.418,0.793,0.669,1.306,0.669
												c0.337,0,0.662-0.108,0.94-0.313l5.2-3.776l5.199,3.776c0.278,0.204,0.604,0.311,0.94,0.311c0.514,0,1.003-0.251,1.306-0.672
												c0.304-0.414,0.386-0.938,0.226-1.435l-1.988-6.121l5.198-3.792c0.568-0.411,0.803-1.141,0.583-1.814
												C36.746,26.781,36.134,26.332,35.439,26.332z M29.985,31.675c-0.564,0.411-0.799,1.135-0.582,1.8l1.829,5.635l-4.789-3.478
												c-0.275-0.202-0.603-0.308-0.943-0.308s-0.667,0.105-0.944,0.308l-4.789,3.478l1.829-5.626c0.216-0.663-0.019-1.386-0.58-1.791
												l-4.835-3.541h5.963c0.714,0,1.313-0.425,1.526-1.078l1.83-5.615l1.83,5.61c0.209,0.648,0.823,1.083,1.527,1.083h5.961
												L29.985,31.675z"/>
											</g>
										</svg>
											</div>
									<div class="hw_thumb_title">
										<h5>
												Audit Service
											</h5> </div>
								</div>
							</li>
							<li class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
								<div class="hw_box_wrap box_wrap_main">
									<div class="hw_thumb"> 
										<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 67 55" enable-background="new 0 0 67 55" xml:space="preserve">
											<g id="_x33_">
												<path d="M59.811,11.817H47.174V7.512c0-3.77-3.09-6.836-6.884-6.836H26.716c-3.794,0-6.882,3.067-6.882,6.836v4.306
												H7.197C3.229,11.817,0,15.025,0,18.967v28.909c0,3.939,3.229,7.149,7.197,7.149h52.614c3.969,0,7.197-3.21,7.197-7.149V18.967
												C67.008,15.025,63.779,11.817,59.811,11.817z M40.648,11.667H26.361V8.251c0-0.787,0.645-1.427,1.438-1.427h11.409
												c0.793,0,1.44,0.64,1.44,1.427V11.667z M21.817,7.512c0-2.683,2.198-4.867,4.899-4.867H40.29c2.701,0,4.9,2.184,4.9,4.867v4.155
												h-2.563V8.251c0-1.874-1.536-3.397-3.42-3.397H27.799c-1.887,0-3.42,1.523-3.42,3.397v3.416h-2.562V7.512z M7.197,13.787h52.614
												c2.874,0,5.214,2.324,5.214,5.181v4.726l-25.227,6.063v-0.715c0-1.356-1.112-2.462-2.48-2.462h-7.63
												c-1.365,0-2.477,1.105-2.477,2.462v0.727L1.982,23.702v-4.735C1.982,16.11,4.321,13.787,7.197,13.787z M37.813,29.041v3.772
												c0,0.271-0.219,0.491-0.495,0.491h-7.63c-0.274,0-0.495-0.221-0.495-0.491v-3.772c0-0.271,0.221-0.493,0.495-0.493h7.63
												C37.594,28.548,37.813,28.77,37.813,29.041z M59.811,53.058H7.197c-2.876,0-5.215-2.325-5.215-5.182V25.728l25.228,6.063v1.022
												c0,1.356,1.112,2.461,2.477,2.461h7.63c1.368,0,2.48-1.104,2.48-2.461v-1.03l25.227-6.063v22.156
												C65.024,50.732,62.685,53.058,59.811,53.058z"/>
												<path d="M60.409,35.602c-0.546,0-0.987,0.442-0.987,0.984v9.934c0,0.552-0.454,1.004-1.015,1.004H52.84
												c-0.547,0-0.99,0.441-0.99,0.984c0,0.545,0.443,0.985,0.99,0.985h5.567c1.652,0,2.995-1.335,2.995-2.974v-9.934
												C61.402,36.044,60.958,35.602,60.409,35.602z"/>
												<path  d="M14.086,47.523H8.518c-0.557,0-1.012-0.452-1.012-1.004v-9.934c0-0.542-0.445-0.984-0.993-0.984
												c-0.546,0-0.988,0.442-0.988,0.984v9.934c0,1.639,1.341,2.974,2.993,2.974h5.568c0.547,0,0.991-0.44,0.991-0.985
												C15.077,47.965,14.633,47.523,14.086,47.523z"/>
											</g>
										</svg>
											</div>
									<div class="hw_thumb_title">
										<h5>
												Taxation
											</h5> </div>
								</div>
							</li>
							<li class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
								<div class="hw_box_wrap box_wrap_main">
									<div class="hw_thumb"> 
										<svg version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										width="64px" height="56px" viewBox="0 0 64 56" enable-background="new 0 0 64 56" xml:space="preserve">
											<g>
												<g>
													<path  d="M42.303,56.019H21.805c-0.653,0-1.182-0.528-1.182-1.181V1.241c0-0.653,0.529-1.183,1.182-1.183h20.498
													c0.653,0,1.184,0.529,1.184,1.183v53.597C43.486,55.49,42.956,56.019,42.303,56.019z M22.986,53.657h18.135V2.423H22.986V53.657z"
													/>
												</g>
												<g>
													<path  d="M32.185,47.938c-2.572,0-4.665-2.091-4.665-4.659c0-2.574,2.092-4.665,4.665-4.665
													c2.573,0,4.666,2.091,4.666,4.665C36.851,45.848,34.758,47.938,32.185,47.938z M32.185,40.982c-1.268,0-2.299,1.028-2.299,2.297
													c0,1.267,1.031,2.297,2.299,2.297s2.298-1.03,2.298-2.297C34.483,42.011,33.453,40.982,32.185,40.982z"/>
												</g>
												<g>
													<path  d="M35.603,11.093h-7.097c-0.653,0-1.183-0.53-1.183-1.183s0.53-1.182,1.183-1.182h7.097
													c0.651,0,1.184,0.529,1.184,1.182S36.254,11.093,35.603,11.093z"/>
												</g>
												<g>
													<path d="M35.603,18.187h-7.097c-0.653,0-1.183-0.529-1.183-1.183c0-0.652,0.53-1.183,1.183-1.183h7.097
													c0.651,0,1.184,0.531,1.184,1.183C36.786,17.657,36.254,18.187,35.603,18.187z"/>
												</g>
												<g>
													<path d="M35.603,25.281h-7.097c-0.653,0-1.183-0.53-1.183-1.182c0-0.653,0.53-1.183,1.183-1.183h7.097
													c0.651,0,1.184,0.529,1.184,1.183C36.786,24.75,36.254,25.281,35.603,25.281z"/>
												</g>
												<g>
													<path  d="M15.103,56.019H1.306c-0.654,0-1.183-0.528-1.183-1.181V1.241c0-0.653,0.529-1.183,1.183-1.183h13.797
													c0.653,0,1.183,0.529,1.183,1.183c0,0.653-0.53,1.183-1.183,1.183H2.487v51.234h12.616c0.653,0,1.183,0.528,1.183,1.181
													S15.756,56.019,15.103,56.019z"/>
												</g>
												<g>
													<path d="M11.686,47.938c-2.572,0-4.665-2.091-4.665-4.659c0-2.574,2.093-4.665,4.665-4.665
													c2.571,0,4.664,2.091,4.664,4.665C16.35,45.848,14.257,47.938,11.686,47.938z M11.686,40.982c-1.269,0-2.3,1.028-2.3,2.297
													c0,1.267,1.032,2.297,2.3,2.297c1.268,0,2.299-1.03,2.299-2.297C13.985,42.011,12.954,40.982,11.686,40.982z"/>
												</g>
												<g>
													<path  d="M15.103,11.093H8.006c-0.653,0-1.183-0.53-1.183-1.183s0.529-1.182,1.183-1.182h7.096
													c0.653,0,1.183,0.529,1.183,1.182S15.756,11.093,15.103,11.093z"/>
												</g>
												<g>
													<path  d="M15.103,18.187H8.006c-0.653,0-1.183-0.529-1.183-1.183c0-0.652,0.529-1.183,1.183-1.183h7.096
													c0.653,0,1.183,0.531,1.183,1.183C16.286,17.657,15.756,18.187,15.103,18.187z"/>
												</g>
												<g>
													<path d="M15.103,25.281H8.006c-0.653,0-1.183-0.53-1.183-1.182c0-0.653,0.529-1.183,1.183-1.183h7.096
													c0.653,0,1.183,0.529,1.183,1.183C16.286,24.75,15.756,25.281,15.103,25.281z"/>
												</g>
												<g>
													<path  d="M62.805,56.019H49.006c-0.653,0-1.183-0.528-1.183-1.181s0.529-1.181,1.183-1.181H61.62V2.423H49.006
													c-0.653,0-1.183-0.53-1.183-1.183c0-0.653,0.529-1.183,1.183-1.183h13.799c0.653,0,1.182,0.529,1.182,1.183v53.597
													C63.986,55.49,63.458,56.019,62.805,56.019z"/>
												</g>
												<g>
													<path d="M52.684,47.938c-2.571,0-4.663-2.091-4.663-4.659c0-2.574,2.092-4.665,4.663-4.665
													c2.574,0,4.665,2.091,4.665,4.665C57.349,45.848,55.258,47.938,52.684,47.938z M52.684,40.982c-1.267,0-2.299,1.028-2.299,2.297
													c0,1.267,1.032,2.297,2.299,2.297c1.269,0,2.302-1.03,2.302-2.297C54.985,42.011,53.952,40.982,52.684,40.982z"/>
												</g>
												<g>
													<path  d="M56.103,11.093h-7.097c-0.653,0-1.183-0.53-1.183-1.183s0.529-1.182,1.183-1.182h7.097
													c0.653,0,1.183,0.529,1.183,1.182S56.756,11.093,56.103,11.093z"/>
												</g>
												<g>
													<path  d="M56.103,18.187h-7.097c-0.653,0-1.183-0.529-1.183-1.183c0-0.652,0.529-1.183,1.183-1.183h7.097
													c0.653,0,1.183,0.531,1.183,1.183C57.285,17.657,56.756,18.187,56.103,18.187z"/>
												</g>
												<g>
													<path d="M56.103,25.281h-7.097c-0.653,0-1.183-0.53-1.183-1.182c0-0.653,0.529-1.183,1.183-1.183h7.097
													c0.653,0,1.183,0.529,1.183,1.183C57.285,24.75,56.756,25.281,56.103,25.281z"/>
												</g>
											</g>
										</svg>  
											</div>
									<div class="hw_thumb_title">
										<h5>
											Other Services
											</h5> </div>
								</div>
							</li>
							
						</ul>
					</div>
					<!--top tab section End-->
					<!--tab content start-->
					<ul class="tab-content">
						<li>
							<div class="row">
								<div class="lw_bottom_description">
									<div class="col-lg-8 col-md-8 col-sm-8">
										<p class="hw_paragraph">Our audit and assurance solutions help clients not only achieve their regulatory objective as attest function but also achieve control over the business objective from an accounting and business perspective.</p>
									</div>
									<div class="col-lg-2 col-sm-4 col-md-4 col-xs-8 col-lg-offset-2 contact_button">
										<div class="lw_button"> <a href="auditing-and-assurance.aspx">
													Know More
												</a> </div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="lw_bottom_description">
									<div class="col-lg-8 col-md-8 col-sm-8">
										<p class="hw_paragraph"> Our taxation consultancy solutions provide absolute control to our clients, enabling them to align their strategies with the constant change in tax policies and regulations. Complex issues are handled effectively by our highly efficient team of professionals. </p>
									</div>
									<div class="col-lg-2 col-sm-4 col-md-4 col-xs-8 col-lg-offset-2 contact_button">
										<div class="lw_button"> <a href="business-taxation.aspx">
													Know More
												</a> </div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="lw_bottom_description">
									<div class="col-lg-8 col-md-8 col-sm-8">
										<p class="hw_paragraph">We assist our clients in all the financial matters including particularly:-              

 

Obtaining Reserve Bank of India approval for setting up offices.</p>
									</div>
									<div class="col-lg-2 col-sm-4 col-md-4 col-xs-8 col-lg-offset-2 contact_button">
										<div class="lw_button"> <a href="other-services.aspx">
													Know More
												</a> </div>
									</div>
								</div>
							</div>
						</li>						
					</ul>
					<!--tab content End-->
				</div>
			</div>
		</div>
	</div>
	<!--tab section End-->

    <!-- carousel_section_start -->
		<div class="wrapper_main client_craousel_wrapper">
			<div class="full_width">
				<div class="full_width client_testimonial">
					
					
					
					<div class="col-lg-6 col-md-6 col-sm-12 client_testimonial_img">
						 <img src="images/vision-mission-values.jpg" style="width:100%;margin-top:25px;" class="img-responsive" alt="mision vision img">
						
					</div>
					
						<div class="col-lg-6 col-md-6 col-sm-12">
						
						<div id="client_testimonial">
							<!--item first start-->
							<div class="item">
                                <h2>Our Mission</h2>
							<p>Our mission to Redefine the professional services landscape by delivering more value than we get with Insightful professional knowledge, achievement-oriented approach, team empowerment, and technology while at the same time providing clients with superior personalized tax, accounting and consulting services by implementing practical solutions for our client's diverse needs.</p>
							
							
						 </div><!--item first end-->
							<!--item second start-->
							<div class="item">
                                  <h2>Our Values</h2>
								<p>BCP Jain & Co. is focused on creating sustainable value growth through our professional expertise and our multidisciplinary skilled term. The values of our company are behind our success and that of our clients. We foster an environment to instill these values in every facet of our organization.</p>
								
                                    <h5>Integrity</h5>
                                    <h5>Accountability</h5>
                                    <h5>Honesty & Trust</h5>
                                    <h5>Upgradation & Innovation</h5>
                                    <h5>Efficiency & Effectiveness  </h5>

								
							</div><!--item second end-->
						</div>
						
					</div>
			
				</div>
				
			</div>
			
		</div>
		<!-- carousel_section_end -->
    <div class="lw_count_wrap_vertical"> 
	 <div class="container">
	 <div class="row">
	     <div class="col-lg-4 col-sm-4 col-md-4 counter_wrapper_main">
		  <!--counter wrapper start-->
		  <div class="full_width">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<svg version="1.1" id="experience" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				width="50px" height="49px" viewBox="0 0 50 49" enable-background="new 0 0 50 49" xml:space="preserve">
					<g>
						
						<polygon fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" points="
						5.542,22.883 24.73,48.252 43.879,22.883 5.542,22.883 	"/>
						
						<polyline fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" points="
						19.137,22.883 18.247,22.883 24.266,45.51 24.675,47.314 24.71,47.18 24.746,47.314 25.155,45.51 31.172,22.883 	"/>
						
						<polyline fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" points="
						5.542,22.563 9.804,14.633 40.225,14.633 43.885,22.563 	"/>
						
						<path fill="none" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" d="
						M14.419,14.633l3.377,7.609c0.621,0.696,0.385,0.696,1.005,0l3.377-7.609"/>
						
						<path fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" d="
						M27.246,14.633l3.377,7.609c0.619,0.696,0.385,0.696,1.006,0l3.377-7.609"/>
						
						<line fill="none" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="24.577" y1="0.748" x2="24.577" y2="6.723"/>
						
						<line fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="11.366" y1="3.047" x2="13.892" y2="8.463"/>
						
						<line fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="0.686" y1="9.883" x2="5.264" y2="13.724"/>
						
						<line fill="none" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="37.844" y1="2.797" x2="35.316" y2="8.213"/>
						
						<line fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="48.68" y1="9.428" x2="44.102" y2="13.27"/>
					</g>
				</svg>
			</div>
			 <div class="col-lg-9 col-md-9 col-sm-12 right_counter_details">
				<h1 class="timer appear_count" data-to="40" data-speed="6000">
					40
				</h1>
				<p>
					year of experience
				</p>
			</div>
		  </div><!--counter wrapper end-->
		</div>
		
		<!--counter wrapper start-->
		<div class="col-lg-4 col-sm-4 col-md-4 counter_wrapper_main">
		 <div class="full_width">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<svg version="1.1" id="professionals" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				width="52px" height="49px" viewBox="0 0 52 49" enable-background="new 0 0 52 49" xml:space="preserve">
					<g>
						
						<path fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" d="
						M34.731,18.112c-1.271,4.403-4.724,7.611-8.007,7.77c-3.292-0.159-6.756-3.388-8.018-7.811l-0.621,0.097
						c-0.702,0.109-1.364-0.376-1.474-1.078l-0.609-3.914c-0.108-0.702,0.377-1.364,1.078-1.474l0.442-0.068
						c-0.197-0.614-0.304-1.266-0.304-1.944v-1.44c0-3.525,2.885-6.41,6.41-6.41h0.764c-0.008-0.121,0.006-0.237,0.048-0.344
						c0.393-1.039,3.022-1.007,5.87,0.072c0.605,0.23,1.174,0.488,1.686,0.763c2.317,0.969,3.956,3.262,3.956,5.919v1.44
						c0,0.678-0.104,1.331-0.302,1.944l0.441,0.068c0.7,0.11,1.186,0.772,1.077,1.474l-0.609,3.914
						c-0.111,0.702-0.771,1.187-1.474,1.078L34.731,18.112L34.731,18.112z"/>
						
						<line fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" x1="41.825" y1="44.537" x2="34.897" y2="44.537"/>
						
						<path fill="none"  stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="22.9256" d="
						M4.102,48.383l-1.15-10.465v-1.099c0-2.444,2.475-3.856,4.541-4.273c1.885-0.383,8.942-1.912,11.225-3.494l0.139-0.02l4.132,19.383
						l2.479-12.787l-2.101-5.415h6.783l-2.1,5.415l2.41,12.787l4.199-19.383l0.141,0.02c2.282,1.582,9.339,3.111,11.224,3.494
						c2.064,0.417,4.54,1.831,4.54,4.273v1.099L49.41,48.383"/>
					</g>
				</svg>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12 right_counter_details">
				<h1 class="timer appear_count" data-to="100" data-speed="7000">
					100+
				</h1>
				<p>
					professionals/Experts
				</p>
			</div>
		 </div><!--counter wrapper end-->
		</div>
		
		<!--counter wrapper start-->
		<div class="col-lg-4 col-sm-4 col-md-4 counter_wrapper_main">
		  <div class="full_width">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<svg version="1.1" id="awards" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				width="40px" height="48px" viewBox="0 0 40 48" enable-background="new 0 0 40 48" xml:space="preserve">
					<g>
						<g>
							<g>
								<path d="M20.055,48h-0.109c-0.529,0-0.979-0.382-1.066-0.902c-0.466-2.793-2.716-3.952-7.765-6.306
								c-1.678-0.782-3.581-1.668-5.713-2.788c-6.798-3.579-4.454-9.951-2.385-15.574c1.07-2.913,2.178-5.923,2.25-8.685
								c0.146-5.688-2.684-7.81-4.382-8.568C0.356,4.942,0.107,4.332,0.318,3.795C0.53,3.256,1.128,2.981,1.674,3.167
								c3.166,1.082,6.136,1.63,8.827,1.63c4.946,0,7.615-1.838,8.47-2.556c0.118-0.244,0.323-0.441,0.587-0.542
								c0.173-0.066,0.354-0.086,0.53-0.062c0.117,0.003,0.233,0.026,0.347,0.069c0.26,0.1,0.469,0.292,0.589,0.53
								c0.849,0.714,3.518,2.561,8.475,2.561c2.693,0,5.66-0.549,8.825-1.63c0.549-0.186,1.146,0.089,1.357,0.628
								c0.212,0.537-0.038,1.147-0.565,1.382c-1.699,0.758-4.529,2.879-4.382,8.568c0.069,2.762,1.179,5.772,2.25,8.685
								c2.069,5.623,4.413,11.995-2.386,15.574c-2.133,1.12-4.034,2.006-5.715,2.788c-5.047,2.354-7.295,3.513-7.763,6.306
								C21.034,47.618,20.583,48,20.055,48z M5.583,6.459C6.679,8.1,7.515,10.455,7.428,13.8c-0.08,3.119-1.251,6.299-2.383,9.377
								c-2.33,6.334-3.51,10.347,1.362,12.911c2.086,1.097,3.964,1.975,5.621,2.746c3.729,1.738,6.516,3.037,7.972,5.255
								c1.456-2.218,4.243-3.517,7.971-5.255c1.656-0.771,3.535-1.649,5.62-2.746c4.876-2.563,3.694-6.576,1.363-12.911
								c-1.131-3.078-2.303-6.258-2.383-9.377c-0.085-3.345,0.75-5.701,1.846-7.341c-1.708,0.332-3.354,0.5-4.918,0.5
								c-5.063,0-8.106-1.715-9.499-2.774c-1.394,1.059-4.437,2.773-9.499,2.774C8.937,6.959,7.292,6.792,5.583,6.459z"/>
							</g>
						</g>
						<g>
							<path d="M7.975,26.425c-1.437,0-2.944-0.079-4.525-0.251c-0.593-0.064-1.022-0.597-0.959-1.189
							c0.065-0.595,0.599-1.024,1.192-0.959c9.142,0.992,16.821-1.033,22.825-6.007c4.492-3.725,6.321-7.808,6.338-7.85
							c0.241-0.545,0.878-0.794,1.426-0.556c0.547,0.24,0.796,0.875,0.557,1.422c-0.08,0.183-2.006,4.524-6.854,8.576
							C24.095,22.852,17.626,26.425,7.975,26.425z"/>
						</g>
						<g>
							<path d="M10.016,31.975c-2.302,0-4.768-0.215-7.408-0.701c-0.588-0.108-0.977-0.672-0.868-1.259
							c0.107-0.588,0.672-0.978,1.259-0.869c17.11,3.15,26.374-5.352,30.372-10.7c0.359-0.477,1.037-0.576,1.515-0.218
							c0.477,0.358,0.575,1.035,0.217,1.514C32.125,23.724,24.231,31.975,10.016,31.975z"/>
						</g>
					</g>
				</svg>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12	right_counter_details">
				<h1 class="timer appear_count" data-to="50000" data-speed="8000">
					50K
				</h1>
				<p>
					Clients Served
				</p>
			</div>
		</div><!--counter wrapper end-->
	 </div>
	 </div><!-- row -->
	</div><!-- container -->
	</div>

    <!-- Our Team Section start-->
		<div class="wrapper_main lw_history_section our_attorney bg_white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h3 class="lw_heading_middle title_home">
							Our Team
						</h3>
					</div>
					<!--first box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/client.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Bal Chand Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: jainamitca@rediffmial.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Bal Chand Jain</h5>
										<h4>FCA</h4>
										
									</div>									
									<a href="contactus.aspx?utm_type=team&utm_id=1">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--first box end-->
					<!--second box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/client.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Amit Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: jainamitca@rediffmial.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Amit Jain</h5>
										<h4>FCA</h4>
										
									</div>
									
									<a href="contactus.aspx?utm_type=team&utm_id=2">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
						</div>
					</div>
					<!--second box end-->
					<!--thired box start-->
					<div class="col-lg-3 col-md-3 col-sm-6 ">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/client.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Rajni Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: cajainrajni@gmail.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Rajni Jain</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=3">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--thired box end-->
					<!--fourth box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/client.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Neelam Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: caneelamjain@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Neelam Jain</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=4">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--fourth box end-->
				</div>
                <div class="full_width lw_middle_buttons">
								<a href="our-team.aspx">View All</a>								
							</div>
			</div>
		</div>
		<!-- Our Team Section End-->
</asp:Content>

