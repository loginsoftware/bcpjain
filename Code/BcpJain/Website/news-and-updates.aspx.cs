﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class news_and_updates : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadNewsUpdate();
    }

    private void LoadNewsUpdate()
    {
        mySql objsql = new mySql();
        GVDetail.DataSource = objsql.LoadAllNewsUpdates();
        GVDetail.DataBind();
    }
}