﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="business-taxation.aspx.cs" Inherits="business_taxation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .mypara {
            color:#fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="wrapper_main lw_slider_section">
		<!-- SLIDER -->
		<div class="example">
			<article class="content" style="width:100%; float:left;">
				<div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
					<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
					<div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul style="width:100% !important;" class="overlay_slide">
							<!--slide-->
							<li data-index="rs-397" data-transition="parallaxhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide Mask" data-description="">
								<!-- MAIN IMAGE --><img src="images/page-banner.png" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 1 -->
									<div class="color_overlay"></div>
								<div class="main_layer" style="text-align:center;">
									<h3 class="tp-caption NotGeneric-Title slideheading  tp-resizeme" id="slide-397-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-75" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap; font-size:40px; line-height: 70px;">
											Business Taxation Services
										</h3>
								
								</div>
							</li>
							<!--slide-->
							<!-- SLIDE  -->
							
						</ul>
					</div>
				</div>
				<!-- END REVOLUTION SLIDER -->
			</article>
		</div>
		<!-- section end -->
		<!-- SLIDER -->
	</div>
    <div class="wrapper_main lw_mission_section history_bg_blue">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							Business &amp; Taxation
						</h3> 
                            <p class="mypara">Our taxation consultancy solutions provide absolute control to our clients, enabling them to align their strategies with the constant change in tax policies and regulations. Complex issues are handled effectively by our highly efficient team of professionals.</p>

				</div>
				<div class="full_width">

					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="left_side_vision_img"> <img src="images/vision_img.png" class="img-responsive" alt="" style="margin-top:-10px;"> </div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="lw_mission_lists">
							<ul>
								<li>Direct and Indirect tax advisory. </li>
								<li> Drafting appeals submission and replies to notices. </li>
								<li>Appearing before adjudication and appellate authorities upto the Tribunal.</li>
								<li> Preparation and filling of various Tax Returns.</li>
								<li>Tax Planning </li>
								<li>Domestic and International Transfer Pricing. </li>
								<li> Complete G.S.T. Assistance. </li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="wrapper_main about_us_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							Business Taxation
						</h3> </div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="about_left_part">
					<p>India offers a well-structured tax system for its population. Taxes are the largest source of income for the government. This money is deployed for various purposes and projects for the development of the nation.</p>
                     <p>Taxes are determined by the Central and State Governments along with local authorities like municipal corporations. The government cannot impose any tax unless it is passed as a law.</p>
                                	</div>
				</div>
				
			</div>
            <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							Types of Tax
						</h3> </div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="about_left_part">
				<p><b>Direct taxes:-</b> Direct taxes are levied on individuals and corporate entities and cannot be transferred to others. As per the Income Tax (IT) Act, 1961 every assessee whose total income exceeds the maximum exempt limit is liable to pay this tax. The tax structure and rates are annually prescribed by the Union Budget. This tax is imposed during each assessment year, which commences on 1st April and ends on 31st March. The total income is calculated from various heads such as business and profession, house property, salaries, capital gains, and other sources. The assesses are classified as individuals, Hindu Undivided Family (HUF), an association of persons (AOP), a body of individuals (BOI), company, firm, local authority, and artificial judiciary not falling in any other category.</p>
                
                        <p><b>Indirect Tax:-</b> As a significant step towards the reform of indirect taxation in India, the Central Government has introduced the Goods and Service Tax (GST). GST is a comprehensive indirect tax on the manufacture, sale, and consumption of goods and services throughout India and will subsume many indirect taxes levied by the Central and State Governments. GST will be implemented through Central GST (CGST), Integrated GST (IGST) and State GST (SGST).Four laws (IGST, CGST, UTGST & GST (Compensation to the States), Act) have received President assent. All the States & UT expected to pass State GST Act, by end of May 2017. GST law is expected to take effect from July  1, 2017.</p>
                                              	</div>
				</div>
				
			</div>
		</div>
	</div>
</asp:Content>

