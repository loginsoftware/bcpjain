﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="contactus.aspx.cs" Inherits="contactus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <!--slider section Start-->
	<div class="wrapper_main lw_header_banner">
		<!-- SLIDER --><img src="images/contact-us-Banner.png" class="img-responsive" alt="banner">
		<!-- section end -->
		<!-- SLIDER -->
	</div>
	<!--wrapper-->
	<!--contact  section Start-->
	<div class="wrapper_main lw_contact_us_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							contact us
						</h3> </div>
			</div>
			<!--main row start-->
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="contact_wrape_box">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
							<svg version="1.1" id="address_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="22px" height="33.917px" viewBox="0 0 22 33.917" enable-background="new 0 0 22 33.917" xml:space="preserve">
								<g>
									<path fill="none"  d="M20.64,15.4l-7.255,15.424c-0.418,0.877-1.335,1.415-2.292,1.415s-1.873-0.538-2.272-1.415
									L1.548,15.4c-0.519-1.097-0.658-2.352-0.658-3.567c0-5.64,4.563-10.203,10.203-10.203c5.639,0,10.204,4.563,10.204,10.203
									C21.297,13.048,21.156,14.304,20.64,15.4z M11.093,6.731c-2.81,0-5.102,2.292-5.102,5.102s2.292,5.102,5.102,5.102
									s5.102-2.292,5.102-5.102S13.903,6.731,11.093,6.731z"/>
								</g>
							</svg>
							</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="lw_contact_right_detalis">
								<h4>
								Bhopal (M.P)
										</h4>
								<p> E-2/33, Arera Colony,

Bhopal, (M.P), 462016.</p>
                                <p> 0755-4273027 
									<br/> +91-930-31-31056
									<br/>+91-942-50-08221 </p>

							</div>
						</div>
					</div>
					<!--contact box end-->
				</div>

                <div class="col-lg-4 col-md-4 col-sm-4">
					<div class="contact_wrape_box">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
							<svg version="1.1" id="address_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="22px" height="33.917px" viewBox="0 0 22 33.917" enable-background="new 0 0 22 33.917" xml:space="preserve">
								<g>
									<path fill="none"  d="M20.64,15.4l-7.255,15.424c-0.418,0.877-1.335,1.415-2.292,1.415s-1.873-0.538-2.272-1.415
									L1.548,15.4c-0.519-1.097-0.658-2.352-0.658-3.567c0-5.64,4.563-10.203,10.203-10.203c5.639,0,10.204,4.563,10.204,10.203
									C21.297,13.048,21.156,14.304,20.64,15.4z M11.093,6.731c-2.81,0-5.102,2.292-5.102,5.102s2.292,5.102,5.102,5.102
									s5.102-2.292,5.102-5.102S13.903,6.731,11.093,6.731z"/>
								</g>
							</svg>
							</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="lw_contact_right_detalis">
								<h4>
								Raipur (C.G)
										</h4>
								<p> 2n Floor, Pithaliya Complex,

K.K. Road, Raipur (C.G.) - 492009</p>
                                <p>0771- 4042-301, 302 
									<br/>+91-930-29-42253

							</div>
						</div>
					</div>
					<!--contact box end-->
				</div>

                <div class="col-lg-4 col-md-4 col-sm-4">
					<div class="contact_wrape_box">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
							<svg version="1.1" id="address_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="22px" height="33.917px" viewBox="0 0 22 33.917" enable-background="new 0 0 22 33.917" xml:space="preserve">
								<g>
									<path fill="none"  d="M20.64,15.4l-7.255,15.424c-0.418,0.877-1.335,1.415-2.292,1.415s-1.873-0.538-2.272-1.415
									L1.548,15.4c-0.519-1.097-0.658-2.352-0.658-3.567c0-5.64,4.563-10.203,10.203-10.203c5.639,0,10.204,4.563,10.204,10.203
									C21.297,13.048,21.156,14.304,20.64,15.4z M11.093,6.731c-2.81,0-5.102,2.292-5.102,5.102s2.292,5.102,5.102,5.102
									s5.102-2.292,5.102-5.102S13.903,6.731,11.093,6.731z"/>
								</g>
							</svg>
							</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="lw_contact_right_detalis">
								<h4>
								Indore (M.P)
										</h4>
								<p> E-39, Anurag Nagar, Makoda Prasnath Apartment, Near Press Complex, A.B. Road, Indore. (M.P)

492001.</p>
                                <p>0731- 2678632/2531374
									<br/>+91-930-21-72979

							</div>
						</div>
					</div>
					<!--contact box end-->
				</div>

                <div class="col-lg-4 col-md-4 col-sm-4">
					<div class="contact_wrape_box">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
							<svg version="1.1" id="address_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="22px" height="33.917px" viewBox="0 0 22 33.917" enable-background="new 0 0 22 33.917" xml:space="preserve">
								<g>
									<path fill="none"  d="M20.64,15.4l-7.255,15.424c-0.418,0.877-1.335,1.415-2.292,1.415s-1.873-0.538-2.272-1.415
									L1.548,15.4c-0.519-1.097-0.658-2.352-0.658-3.567c0-5.64,4.563-10.203,10.203-10.203c5.639,0,10.204,4.563,10.204,10.203
									C21.297,13.048,21.156,14.304,20.64,15.4z M11.093,6.731c-2.81,0-5.102,2.292-5.102,5.102s2.292,5.102,5.102,5.102
									s5.102-2.292,5.102-5.102S13.903,6.731,11.093,6.731z"/>
								</g>
							</svg>
							</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="lw_contact_right_detalis">
								<h4>
								Vidisha (M.P)
										</h4>
								<p>C/o S.P. Jain , Kahan Samyak Tallaya Vidisha, Vidisha

(M.P)- 464001</p>
                                <p>+91-898-95-42920

							</div>
						</div>
					</div>
					<!--contact box end-->
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="contact_wrape_box">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                           <i class="fa fa-envelope-o">
									</i> </div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="lw_contact_right_detalis">
								<h4>
											email
										</h4>
								<p> info@bcpjain.com </p>
							</div>
						</div>
					</div>
					<!--contact box end-->
				</div>
				
			</div>
			<!--main row start-->
		</div>
	</div>
	<!--contact  section End-->
	<!-- map section start-->
	<div class="wrapper_main contact_map_section">
		<div class="lw_map_part">
			<div id="contact_google_map"></div>
		</div>
	</div>
	<!-- map section End-->
	<!--contact form section start-->
	<div class="wrapper_main contact_form_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_form_heading">
						<h5>Quick</h5>
						<h3>Contact form</h3> </div>
				</div>
				<div class="lw_contact_form">
					<div >
						<div class="col-lg-6 col-md-6 col-sm-6">
							<input type="text" name="name" placeholder="Name*" class="contact_input"/> </div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<input type="email" name="email" placeholder="Email*"  class="contact_input" /> </div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<input type="text" name="phone" placeholder="Phone*"  class="contact_input"/> </div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<input type="text" name="subject" placeholder="Subject*"  class="contact_input" />
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
						 <textarea name="message" class="contact_textarea" placeholder="Message*" id="mesg"></textarea>
						</div>
						<div class="submit-btn-frm col-lg-12">
        <asp:Button ID="BtnSend" CssClass="btn btn-success" runat="server" Text="Submit Now" OnClick="BtnSend_Click" />
							</div>
					</div>
                                                      <div class="col-lg-12">
							<p id="err"></p>
                                                       </div>
				</div>
			</div>
		</div>
	</div>
	<!--contact form end-->
    <!--custom js-->
	
</asp:Content>

