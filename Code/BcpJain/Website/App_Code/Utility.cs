﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.UI;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public string DomainUrl()
    {
        return "http://wealthaims.com/";
    }


    public void SendMail(string RecipientEmail, string subjectmail, string bodymail)
    {

        string _senderEmail = "loginsoftware.in@gmail.com";
        string _senderPassword = "vidisha@0755";
        MailMessage Msg = new MailMessage();
        // Sender e-mail address.
        Msg.From = new MailAddress(_senderEmail);
        // Recipient e-mail address.
        Msg.To.Add(RecipientEmail);
        Msg.Subject = subjectmail;
        // File Upload path
        //String FileName = fileUpload1.PostedFile.FileName;
        string mailbody = bodymail + "<br/><img src=cid:companylogo>";
        //LinkedResource myimage = new LinkedResource(FileName);
        // Create HTML view
        AlternateView htmlMail = AlternateView.CreateAlternateViewFromString(mailbody, null, "text/html");

        Msg.AlternateViews.Add(htmlMail);
        // your remote SMTP server IP.
        SmtpClient smtp = new SmtpClient();
        smtp.Host = "smtp.gmail.com";
        smtp.Port = 25;
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = new System.Net.NetworkCredential(_senderEmail, _senderPassword);
        smtp.EnableSsl = true;
        smtp.Send(Msg);
        Msg = null;
    }

    public string CheckFileNameExist(string FolderNm, string FileNm)
    {
        FileNm = RemoveWhiteSpaceFromString(FileNm);
        string[] NewFileNm;
        int i = 1;

        NewFileNm = FileNm.Split('.');

        while (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/" + FolderNm + "/" + FileNm)) == true)
        {
            i = i + 1;
            FileNm = NewFileNm[0] + i.ToString() + "." + NewFileNm[1];
        }
        return FileNm;
    }

    public string RemoveWhiteSpaceFromString(string _string)
    {
        _string = System.Text.RegularExpressions.Regex.Replace(_string, @"[\s+]", "");
        return _string;
    }

    public void ShowMessage(string msg, Page page)
    {
        msg = msg.ToString().Replace("'", " ").Replace("\r\n", " ");

        if (msg.Contains("UK_"))
        {
            msg = "Data already exist !";
        }

        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('" + msg + "');", true);
    }

    public void SetCookie(Page myPage, string CookieName, string CookieValue)
    {
        HttpCookie CN = new HttpCookie(CookieName);

        CN.Value = CookieValue;

        if (CookieValue == null)
        {
            // myPage.Response.Cookies.Remove(CookieName);
            myPage.Response.Cookies[CookieName].Expires = DateTime.Now.AddDays(-1);
        }
        else
        {
            myPage.Response.Cookies.Add(CN);
        }

    }

    public string GetCookie(Page myPage, string CookieName)
    {
        string cookieValue = "";
        try
        {
            cookieValue = myPage.Request.Cookies[CookieName].Value;
        }
        catch (Exception ex)
        {
            cookieValue = null;
        }
        return cookieValue;
    }
}