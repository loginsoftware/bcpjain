﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for mySql
/// </summary>
public class mySql
{
    String strConnString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;  
  
    public DataTable LoginUser(string UserName)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_LoginDetailLoadByUserName", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@UserName", UserName);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadUserDetail()
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_UserDetailLoadAll", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public int InsertLoginDetail(string FullName, string UserName, string Password, string UserType)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_LoginDetailInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@FullName", FullName);
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@Password", Password);
                cmd.Parameters.AddWithValue("@UserType", UserType);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.Add("@UserId", SqlDbType.Int);
                cmd.Parameters["@UserId"].Direction = ParameterDirection.Output;  

              _result =  cmd.ExecuteNonQuery();
              _result = (int)cmd.Parameters["@UserId"].Value;  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int InsertLoginDetailUpdate(int UserId, string FullName, string UserName, string Password, string UserType)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_LoginDetailUpdate", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@FullName", FullName);
                cmd.Parameters.AddWithValue("@UserName", UserName);
                cmd.Parameters.AddWithValue("@Password", Password);
                cmd.Parameters.AddWithValue("@UserType", UserType);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);

                _result = cmd.ExecuteNonQuery();
                _result = (int)cmd.Parameters["@UserId"].Value;  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int InsertEmpMaster(string EmployeeName, string Designation, string EmailId, string MobileNo, string Address, DateTime CreatedOn, int UserId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_EmpMasterInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EmployeeName", EmployeeName);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@EmailId", EmailId);
                cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.Add("@EmployeeId", SqlDbType.Int);
                cmd.Parameters["@EmployeeId"].Direction = ParameterDirection.Output;

                _result = cmd.ExecuteNonQuery();
                _result = (int)cmd.Parameters["@EmployeeId"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int InsertEmpMasterUpdate(int EmployeeId, string EmployeeName, string Designation, string EmailId, string MobileNo, string Address, DateTime CreatedOn, int UserId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_EmpMasterUpdate", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                cmd.Parameters.AddWithValue("@EmployeeName", EmployeeName);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@EmailId", EmailId);
                cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UserId", UserId);

                _result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return EmployeeId;
    }

  
    public DataTable LoadAllEmployeeDetail()
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_EmpMasterLoadAllV", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadAllEmployeByEmpId(int EmpId)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_EmpMasterLoadByEmpId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId", EmpId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }


    public int InsertClientMaster(string FullName, string Designation, string EmailId, string MobileNo, string Address, DateTime CreatedOn, int UserId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_ClientMasterInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@FullName", FullName);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@EmailId", EmailId);
                cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.Add("@ClientId", SqlDbType.Int);
                cmd.Parameters["@ClientId"].Direction = ParameterDirection.Output;

                _result = cmd.ExecuteNonQuery();
                _result = (int)cmd.Parameters["@ClientId"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int InsertClientMasterUpdate(int ClientId, string FullName, string Designation, string EmailId, string MobileNo, string Address, DateTime CreatedOn, int UserId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_ClientMasterUpdate", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ClientId", ClientId);
                cmd.Parameters.AddWithValue("@FullName", FullName);
                cmd.Parameters.AddWithValue("@Designation", Designation);
                cmd.Parameters.AddWithValue("@EmailId", EmailId);
                cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UserId", UserId);

                _result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return ClientId;
    }


    public DataTable LoadAllClientDetail()
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_ClientMasterLoadAllV", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadAllClientByClientId(int ClientId)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_ClientMasterLoadByClientId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ClientId", ClientId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadClientByUserId(int UserId)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_ClientMasterLoadByUserId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }


    public int InsertNewsUpdate(string Description, string FilePath)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_NewsUpdateInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.Parameters.AddWithValue("@FilePath", FilePath);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.Add("@NewsUpdateId", SqlDbType.Int);
                cmd.Parameters["@NewsUpdateId"].Direction = ParameterDirection.Output;

                _result = cmd.ExecuteNonQuery();
                _result = (int)cmd.Parameters["@NewsUpdateId"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public DataTable LoadAllNewsUpdates()
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_NewsUpdateLoadAll", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public int DeleteNewsUpdates(int NewsUpdateId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_NewsUpdateDelete", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewsUpdateId", NewsUpdateId);

                _result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int InsertTaskMaster(string Description, string FilePath, DateTime CreatedOn, int FromUserId, int SentToUserId, bool IsDownload, string Status, int CreatedByUserId)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_TaskMasterInsert", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.Parameters.AddWithValue("@FilePath", FilePath);
                cmd.Parameters.AddWithValue("@CreatedOn", CreatedOn);
                cmd.Parameters.AddWithValue("@FromUserId", FromUserId);
                cmd.Parameters.AddWithValue("@SentToUserId", SentToUserId);
                cmd.Parameters.AddWithValue("@IsDownload", IsDownload);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@CreatedByUserId", CreatedByUserId);
                cmd.Parameters.Add("@TaskId", SqlDbType.Int);
                cmd.Parameters["@TaskId"].Direction = ParameterDirection.Output;

                _result = cmd.ExecuteNonQuery();
                _result = (int)cmd.Parameters["@TaskId"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }

    public int UpdateTaskAssignedToEmployee(int TaskId, string Description, string FilePath, int FromUserId, int SentToUserId, string Status)
    {
        int _result = 0;
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_TaskMasterUpdateAssignedToEmp", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@TaskId", TaskId);
                cmd.Parameters.AddWithValue("@Description", Description);
                cmd.Parameters.AddWithValue("@FilePath", FilePath);
                cmd.Parameters.AddWithValue("@FromUserId", FromUserId);
                cmd.Parameters.AddWithValue("@SentToUserId", SentToUserId);
                cmd.Parameters.AddWithValue("@Status", Status);

                _result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return _result;
    }
    public DataTable LoadAllTaskFromClient()
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_TaskMasterLoadByClient", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadTaskByTaskId(int TaskId)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_TaskMasterLoadByPrimaryKey", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TaskId", TaskId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

    public DataTable LoadTaskBySentToUserId(int SentToUserId)
    {
        DataTable Dt = new DataTable();
        DataSet Ds = new DataSet();
        using (SqlConnection con = new SqlConnection(strConnString))
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("proc_TaskMasterLoadBySentToUserId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SentToUserId", SentToUserId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(Ds);
                Dt = Ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        return Dt;
    }

}
	
