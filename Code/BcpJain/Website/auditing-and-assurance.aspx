﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="auditing-and-assurance.aspx.cs" Inherits="auditing_and_assurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .mypara {
            color:#fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="wrapper_main lw_slider_section">
		<!-- SLIDER -->
		<div class="example">
			<article class="content" style="width:100%; float:left;">
				<div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
					<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
					<div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul style="width:100% !important;" class="overlay_slide">
							<!--slide-->
							<li data-index="rs-397" data-transition="parallaxhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide Mask" data-description="">
								<!-- MAIN IMAGE --><img src="images/page-banner.png" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 1 -->
									<div class="color_overlay"></div>
								<div class="main_layer" style="text-align:center;">
									<h3 class="tp-caption NotGeneric-Title slideheading  tp-resizeme" id="slide-397-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-75" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap; font-size:40px; line-height: 70px;">
											Aduting & Assurance Services
										</h3>
								
								</div>
							</li>
							<!--slide-->
							<!-- SLIDE  -->
							
						</ul>
					</div>
				</div>
				<!-- END REVOLUTION SLIDER -->
			</article>
		</div>
		<!-- section end -->
		<!-- SLIDER -->
	</div>
    <div class="wrapper_main lw_mission_section history_bg_blue">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							Auditing &amp; Assurance
						</h3> 
                            <p class="mypara">Our audit and assurance solutions help clients not only achieve their regulatory objective as attest function but also achieve control over the business objective from an accounting and business perspective. We offer sector-wise in-depth analysis of financial statements for SMEs, private and public companies, partnerships firms, Not-for-profit organizations and all other forms of businesses, including:-</p>

				</div>
				<div class="full_width">

					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="left_side_vision_img"> <img src="images/vision_img.png" class="img-responsive" alt="" style="margin-top:-10px;"> </div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="lw_mission_lists">
							<ul>
								<li>Statutory Audit under Corporate and Income Tax Laws. </li>
								<li> Reviews of annual and quarterly financial statements, which includes some analytical procedures and limited assurance. </li>
								<li> Compilations of annual and quarterly financial statements primarily for internal management use. </li>
								<li> Statutory Audit under Societies and Trust law. </li>
								<li>Special Audits u/s 142 (2A) of the Income Tax Act, 1961. </li>
								<li>Due Diligence Audit </li>
								<li> EDP Audit. </li>
								<li> Internal/Concurrent Audits. </li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

