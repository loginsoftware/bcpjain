﻿<%@ Page Title="About Us | BCP Jain & Company | CHARTERED ACCOUNTANTS" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="aboutus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="wrapper_main lw_header_banner">
		<!-- SLIDER --><img src="images/aboutus.png" class="img-responsive" alt="banner">
      
		<!-- section end -->
		<!-- SLIDER -->
	</div>
    <div class="wrapper_main about_us_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							about us
						</h3> </div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="about_left_part">
						<p> B.C.P Jain & Co. is a prominent Chartered Accountancy firm in Bhopal, led by skilled and experienced Indian Chartered Accountants having industry experience of over a decade. Our vast knowledge, expertise and core competence in various areas helps us to serve our client’s varied needs and achieve Total Client Satisfaction.​​​ </p>
						<p>The head office is located at utmost modern,  high profile and fastest developing the business center of Central India Bhopal (MP) & Raipur (CG)  having the office in about  3000 sq ft  & 1500 sq ft area respectively. The firm is having 40 years of grueling, hardcore and penetrating experience in the areas of Audits, Corporate Law, Taxation (Direct and Indirect), Accounting, Management Consultancy, FCRA Consultancy, Consultancy of Funds Management,  Investment and Portfolio Management, BPO work and Consultancy on Costing Matters etc. The firm is predominantly in practice of Taxations & Audits of Companies and other entities.    </p>
                        <p>We are here to help your business in every manner for making your Accounts and Tax Departments more efficient and transparent; and thereby making them reflect true mirror of your business</p>
                       	</div>
				</div>
				
			</div>
		</div>
	</div>

    <div class="wrapper_main lw_client_section">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-8 col-md-offset-1">
						<div class="lw_blockqoute_section">
							<h3 class="lw_heading_middle title_home">
									Founder of BCP Jain & Co.
								</h3>
							<div class="blockquote_slider owl-carousel owl-theme" id="blockquote_slider" style="opacity: 1; display: block;">
								<!--item first start-->
								<div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 3272px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 818px;"><div class="item"> <i class="fa fa-quote-left">
										</i>
									<p> "Develop a passion for learning if you haven't inherited it naturally.

If you do, you will never cease to grow. In hindsight, all our effort

and struggle to be worth it, especially when it is identified and appreciated  by our clients." </p> <i class="fa fa-quote-right">
										</i>
									<div class="lw_bloquote_img full_width">
										<div class="bloq_circle_image"> <img src="images/team1.png" alt="" class="img-circle"> </div>
										<div class="client_description">
											<div class="client_title"> Bal Chand Jain </div> <span>
													Founder
												</span> </div>
									</div>
								</div></div>
                                    
                                   </div></div>
								<!--item first End-->
								<!--item first Start-->
								
								<!--item first End-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

