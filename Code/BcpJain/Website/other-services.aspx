﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="other-services.aspx.cs" Inherits="other_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .mypara {
            color:#fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <div class="wrapper_main lw_slider_section">
		<!-- SLIDER -->
		<div class="example">
			<article class="content" style="width:100%; float:left;">
				<div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
					<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
					<div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul style="width:100% !important;" class="overlay_slide">
							<!--slide-->
							<li data-index="rs-397" data-transition="parallaxhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide Mask" data-description="">
								<!-- MAIN IMAGE --><img src="images/page-banner.png" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYER NR. 1 -->
									<div class="color_overlay"></div>
								<div class="main_layer" style="text-align:center;">
									<h3 class="tp-caption NotGeneric-Title slideheading  tp-resizeme" id="slide-397-layer-1" data-x="center" data-hoffset="" data-y="center" data-voffset="-75" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap; font-size:40px; line-height: 70px;">
											Other Services we Provide
										</h3>
								
								</div>
							</li>
							<!--slide-->
							<!-- SLIDE  -->
							
						</ul>
					</div>
				</div>
				<!-- END REVOLUTION SLIDER -->
			</article>
		</div>
		<!-- section end -->
		<!-- SLIDER -->
	</div>
    <div class="wrapper_main lw_mission_section history_bg_blue">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="lw_heading_middle">
							Other Services
						</h3>                             
				</div>
				<div class="full_width">
                            <p class="mypara">We assist our clients in all the financial matters including particularly:-</p>
					<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="lw_mission_lists">
							<ul>
								<li>Obtaining Reserve Bank of India approval for setting up offices. </li>
								<li>Assisting in incorporating companies/LLP’s branch offices. </li>
								<li> Obtaining and handling all Business Registrations. </li>
								<li> Company Incorporation & Registration. </li>
								<li>Assistance in Export/Import Licence. </li>
								<li>Handling periodic legal Compliance.</li>
								<li> Other related services as needed.</li>
								<li> Corporate Advisory Services. </li>
                               
							</ul>
							
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="lw_mission_lists">
							<ul>
								
                                <li>Business Valuation Services.</li>
                                <li>Handling payroll Services.</li>
                                <li>Management Consultancy.</li>
                                <li>Forensic Audit Services</li>
                                <li>Accounts Outsourcing.</li>
                                <li>E.R.P Consultancy.</li>
                                <li>Project Financing.</li>
                                <li>FCRA.</li>
                                 <br />
                    <br />
                    <br />
                    <br />
							</ul>
							
						</div>
					</div>
                   
				</div>
			</div>
		</div>
	</div>
</asp:Content>

