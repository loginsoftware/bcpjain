﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="our-team.aspx.cs" Inherits="our_team" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph" Runat="Server">
    <!-- Our Team Section start-->
		<div class="wrapper_main lw_history_section our_attorney bg_white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h3 class="lw_heading_middle title_home">
							Our Team
						</h3>
					</div>
					<!--first box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team1.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Bal Chand Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: balchand@bcpjain.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Bal Chand Jain</h5>
										<h4>FCA</h4>
										
									</div>									
									<a href="contactus.aspx?utm_type=team&utm_id=1">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--first box end-->
					<!--second box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team2.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Amit Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: jainamitca@rediffmail.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Amit Jain</h5>
										<h4>FCA</h4>
										
									</div>
									
									<a href="contactus.aspx?utm_type=team&utm_id=2">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
						</div>
					</div>
					<!--second box end-->
					<!--thired box start-->
					<div class="col-lg-3 col-md-3 col-sm-6 ">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team3.png" class="img-responsive" alt="">
								<div class="hw_thumb_title">
									<h5>
										CA. Rajni Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: cajainrajni@gmail.com</p>
								</div>
								  <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Rajni Jain</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=3">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--thired box end-->
					<!--fourth box start-->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team4.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Neelam Jain
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: caneelamjain@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Neelam Jain</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=4">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>
					<!--fourth box end-->



                    <div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team5.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Amit Chopra
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: amit.chopra.ca@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Amit Chopra</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=5">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team6.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Pankaj Agrawal
									</h5>
									<p>
										FCA
									</p>
                                    <p>Email: agrawalpankajca@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Pankaj Agrawal</h5>
										<h4>FCA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=6">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team7.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Rahul Jain
									</h5>
									<p>
										ACA
									</p>
                                    <p>Email: jncarahul@gmail.com </p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Rahul Jain</h5>
										<h4>ACA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=7">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team8.png" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Naresh Kumar Vyas
									</h5>
									<p>
										ACA
									</p>
                                    <p>Email: agrawalpankajca@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Naresh Kumar Vyas</h5>
										<h4>ACA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=8">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team10.jpeg" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Siddhant Jain
									</h5>
									<p>
										ACA
									</p>
                                    <p>Email: casiddant9@gmail.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Siddhant Jain</h5>
										<h4>ACA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=9">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>

                    <%--<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="attorney_img_wrap">
							<div class="attorney_img">
								<img src="images/team10.jpeg" class="img-responsive" alt="">
								
								<div class="hw_thumb_title">
									<h5>
										CA. Ashutosh Loya
									</h5>
									<p>
										ACA
									</p>
                                    <p>Email: ashutosh@bcpjain.com</p>
								</div>
								 <!-- overlay start -->
								<div class="overlay_box">
									<div class="hw_thumb_title_second">
										<h5>CA. Ashutosh Loya</h5>
										<h4>ACA</h4>
										
									</div>
									<a href="contactus.aspx?utm_type=team&utm_id=10">contact us</a>
								</div><!-- overlay end -->
							</div> <!-- img end -->
							
						</div>
					</div>--%>
				</div>
                
			</div>
		</div>
		<!-- Our Team Section End-->
</asp:Content>

