
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClientMasterDelete]
(
	@ClientId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [ClientMaster]
	WHERE
		[ClientId] = @ClientId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClientMasterInsert]
(
	@ClientId int = NULL output,
	@FullName nvarchar(50),
	@Designation nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Address nvarchar(200),
	@CreatedOn datetime,
	@UserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [ClientMaster]
	(
		[FullName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	)
	VALUES
	(
		@FullName,
		@Designation,
		@EmailId,
		@MobileNo,
		@Address,
		@CreatedOn,
		@UserId
	)

	SET @Err = @@Error

	SELECT @ClientId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClientMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ClientId],
		[FullName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	FROM [ClientMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterLoadAllV]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_ClientMasterLoadAllV]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VClient] order by ClientId desc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterLoadByClientId]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_ClientMasterLoadByClientId]
(
@ClientId int
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VClient] where @ClientId = ClientId

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClientMasterLoadByPrimaryKey]
(
	@ClientId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[ClientId],
		[FullName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	FROM [ClientMaster]
	WHERE
		([ClientId] = @ClientId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterLoadByUserId]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_ClientMasterLoadByUserId]
(
@UserId int
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VClient] where @UserId = UserId

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_ClientMasterUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_ClientMasterUpdate]
(
	@ClientId int,
	@FullName nvarchar(50),
	@Designation nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Address nvarchar(200),
	@CreatedOn datetime,
	@UserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [ClientMaster]
	SET
		[FullName] = @FullName,
		[Designation] = @Designation,
		[EmailId] = @EmailId,
		[MobileNo] = @MobileNo,
		[Address] = @Address,
		[CreatedOn] = @CreatedOn,
		[UserId] = @UserId
	WHERE
		[ClientId] = @ClientId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_EmpMasterDelete]
(
	@EmployeeId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [EmpMaster]
	WHERE
		[EmployeeId] = @EmployeeId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_EmpMasterInsert]
(
	@EmployeeId int = NULL output,
	@EmployeeName nvarchar(50),
	@Designation nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Address nvarchar(200),
	@CreatedOn datetime,
	@UserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [EmpMaster]
	(
		[EmployeeName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	)
	VALUES
	(
		@EmployeeName,
		@Designation,
		@EmailId,
		@MobileNo,
		@Address,
		@CreatedOn,
		@UserId
	)

	SET @Err = @@Error

	SELECT @EmployeeId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_EmpMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[EmployeeId],
		[EmployeeName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	FROM [EmpMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterLoadAllV]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_EmpMasterLoadAllV]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VEmp] order by EmployeeId desc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterLoadByEmpId]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_EmpMasterLoadByEmpId]
(
  @EmployeeId int
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VEmp] where EmployeeId = @EmployeeId

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_EmpMasterLoadByPrimaryKey]
(
	@EmployeeId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[EmployeeId],
		[EmployeeName],
		[Designation],
		[EmailId],
		[MobileNo],
		[Address],
		[CreatedOn],
		[UserId]
	FROM [EmpMaster]
	WHERE
		([EmployeeId] = @EmployeeId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_EmpMasterUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_EmpMasterUpdate]
(
	@EmployeeId int,
	@EmployeeName nvarchar(50),
	@Designation nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Address nvarchar(200),
	@CreatedOn datetime,
	@UserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [EmpMaster]
	SET
		[EmployeeName] = @EmployeeName,
		[Designation] = @Designation,
		[EmailId] = @EmailId,
		[MobileNo] = @MobileNo,
		[Address] = @Address,
		[CreatedOn] = @CreatedOn,
		[UserId] = @UserId
	WHERE
		[EmployeeId] = @EmployeeId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_InboxDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_InboxDelete]
(
	@InboxId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [Inbox]
	WHERE
		[InboxId] = @InboxId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_InboxInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_InboxInsert]
(
	@InboxId int = NULL output,
	@Name nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Subject nvarchar(100),
	@Message nvarchar(MAX),
	@EmployeeId int = NULL,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [Inbox]
	(
		[Name],
		[EmailId],
		[MobileNo],
		[Subject],
		[Message],
		[EmployeeId],
		[CreatedOn]
	)
	VALUES
	(
		@Name,
		@EmailId,
		@MobileNo,
		@Subject,
		@Message,
		@EmployeeId,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @InboxId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_InboxLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_InboxLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[InboxId],
		[Name],
		[EmailId],
		[MobileNo],
		[Subject],
		[Message],
		[EmployeeId],
		[CreatedOn]
	FROM [Inbox]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_InboxLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_InboxLoadByPrimaryKey]
(
	@InboxId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[InboxId],
		[Name],
		[EmailId],
		[MobileNo],
		[Subject],
		[Message],
		[EmployeeId],
		[CreatedOn]
	FROM [Inbox]
	WHERE
		([InboxId] = @InboxId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_InboxUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_InboxUpdate]
(
	@InboxId int,
	@Name nvarchar(50),
	@EmailId nvarchar(50),
	@MobileNo nvarchar(50),
	@Subject nvarchar(100),
	@Message nvarchar(MAX),
	@EmployeeId int = NULL,
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [Inbox]
	SET
		[Name] = @Name,
		[EmailId] = @EmailId,
		[MobileNo] = @MobileNo,
		[Subject] = @Subject,
		[Message] = @Message,
		[EmployeeId] = @EmployeeId,
		[CreatedOn] = @CreatedOn
	WHERE
		[InboxId] = @InboxId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailDelete]
(
	@UserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [LoginDetail]
	WHERE
		[UserId] = @UserId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailInsert]
(
	@UserId int = NULL output,
	@FullName nvarchar(50),
	@UserName nvarchar(50),
	@Password nvarchar(50),
	@UserType nvarchar(50),
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [LoginDetail]
	(
		[FullName],
		[UserName],
		[Password],
		[UserType],
		[CreatedOn]
	)
	VALUES
	(
		@FullName,
		@UserName,
		@Password,
		@UserType,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @UserId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[UserId],
		[FullName],
		[UserName],
		[Password],
		[UserType],
		[CreatedOn]
	FROM [LoginDetail]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailLoadByPrimaryKey]
(
	@UserId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[UserId],
		[FullName],
		[UserName],
		[Password],
		[UserType],
		[CreatedOn]
	FROM [LoginDetail]
	WHERE
		([UserId] = @UserId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailLoadByUserName]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_LoginDetailLoadByUserName]
(
	@UserName nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [LoginDetail]
	WHERE
		([UserName] = @UserName)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_LoginDetailUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_LoginDetailUpdate]
(
	@UserId int,
	@FullName nvarchar(50),
	@UserName nvarchar(50),
	@Password nvarchar(50),
	@UserType nvarchar(50),
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [LoginDetail]
	SET
		[FullName] = @FullName,
		[UserName] = @UserName,
		[Password] = @Password,
		[UserType] = @UserType,
		[CreatedOn] = @CreatedOn
	WHERE
		[UserId] = @UserId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_NewsUpdateDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_NewsUpdateDelete]
(
	@NewsUpdateId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [NewsUpdate]
	WHERE
		[NewsUpdateId] = @NewsUpdateId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_NewsUpdateInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_NewsUpdateInsert]
(
	@NewsUpdateId int = NULL output,
	@Description nvarchar(200),
	@FilePath nvarchar(MAX),
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [NewsUpdate]
	(
		[Description],
		[FilePath],
		[CreatedOn]
	)
	VALUES
	(
		@Description,
		@FilePath,
		@CreatedOn
	)

	SET @Err = @@Error

	SELECT @NewsUpdateId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_NewsUpdateLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_NewsUpdateLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[NewsUpdateId],
		[Description],
		[FilePath],
		[CreatedOn]
	FROM [NewsUpdate]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_NewsUpdateLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_NewsUpdateLoadByPrimaryKey]
(
	@NewsUpdateId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[NewsUpdateId],
		[Description],
		[FilePath],
		[CreatedOn]
	FROM [NewsUpdate]
	WHERE
		([NewsUpdateId] = @NewsUpdateId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_NewsUpdateUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_NewsUpdateUpdate]
(
	@NewsUpdateId int,
	@Description nvarchar(200),
	@FilePath nvarchar(MAX),
	@CreatedOn datetime
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [NewsUpdate]
	SET
		[Description] = @Description,
		[FilePath] = @FilePath,
		[CreatedOn] = @CreatedOn
	WHERE
		[NewsUpdateId] = @NewsUpdateId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterDelete]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterDelete]
(
	@TaskId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [TaskMaster]
	WHERE
		[TaskId] = @TaskId
	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterInsert]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterInsert]
(
	@TaskId int = NULL output,
	@Description nvarchar(MAX),
	@FilePath nvarchar(MAX),
	@CreatedOn datetime,
	@FromUserId int,
	@SentToUserId int,
	@IsDownload bit,
	@Status nvarchar(50),
	@CreatedByUserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [TaskMaster]
	(
		[Description],
		[FilePath],
		[CreatedOn],
		[FromUserId],
		[SentToUserId],
		[IsDownload],
		[Status],
		[CreatedByUserId]
	)
	VALUES
	(
		@Description,
		@FilePath,
		@CreatedOn,
		@FromUserId,
		@SentToUserId,
		@IsDownload,
		@Status,
		@CreatedByUserId
	)

	SET @Err = @@Error

	SELECT @TaskId = SCOPE_IDENTITY()

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterLoadAll]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[TaskId],
		[Description],
		[FilePath],
		[CreatedOn],
		[FromUserId],
		[SentToUserId],
		[IsDownload],
		[Status],
		[CreatedByUserId]
	FROM [TaskMaster]

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterLoadByClient]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterLoadByClient]


AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VTaskMasterClient] where UserType = 'Client'  order by TaskId Desc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterLoadByPrimaryKey]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterLoadByPrimaryKey]
(
	@TaskId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[TaskId],
		[Description],
		[FilePath],
		[CreatedOn],
		[FromUserId],
		[SentToUserId],
		[IsDownload],
		[Status],
		[CreatedByUserId]
	FROM [TaskMaster]
	WHERE
		([TaskId] = @TaskId)

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterLoadBySentToUserId]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[proc_TaskMasterLoadBySentToUserId]
(
	@SentToUserId int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT *
	FROM [VTaskMasterClient] where SentToUserId = @SentToUserId order by TaskId Desc

	SET @Err = @@Error

	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterUpdate]
(
	@TaskId int,
	@Description nvarchar(MAX),
	@FilePath nvarchar(MAX),
	@CreatedOn datetime,
	@FromUserId int,
	@SentToUserId int,
	@IsDownload bit,
	@Status nvarchar(50),
	@CreatedByUserId int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [TaskMaster]
	SET
		[Description] = @Description,
		[FilePath] = @FilePath,
		[CreatedOn] = @CreatedOn,
		[FromUserId] = @FromUserId,
		[SentToUserId] = @SentToUserId,
		[IsDownload] = @IsDownload,
		[Status] = @Status,
		[CreatedByUserId] = @CreatedByUserId
	WHERE
		[TaskId] = @TaskId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  StoredProcedure [dbo].[proc_TaskMasterUpdateAssignedToEmp]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[proc_TaskMasterUpdateAssignedToEmp]
(
	@TaskId int = NULL output,
	@Description nvarchar(MAX),
	@FilePath nvarchar(MAX),
	@FromUserId int,
	@SentToUserId int,
	@Status nvarchar(50)
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [TaskMaster]
	SET
		[Description] = @Description,
		[FilePath] = @FilePath,
		[FromUserId] = @FromUserId,
		[SentToUserId] = @SentToUserId,
		[Status] = @Status
	WHERE
		[TaskId] = @TaskId


	SET @Err = @@Error


	RETURN @Err
END

GO
/****** Object:  Table [dbo].[ClientMaster]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientMaster](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[Designation] [nvarchar](50) NOT NULL,
	[EmailId] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_ClientMaster] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmpMaster]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpMaster](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [nvarchar](50) NOT NULL,
	[Designation] [nvarchar](50) NOT NULL,
	[EmailId] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_EmpMaster] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inbox]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inbox](
	[InboxId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[EmailId] [nvarchar](50) NOT NULL,
	[MobileNo] [nvarchar](50) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[EmployeeId] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Inbox] PRIMARY KEY CLUSTERED 
(
	[InboxId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoginDetail]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginDetail](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[UserType] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_LoginDetail] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsUpdate]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsUpdate](
	[NewsUpdateId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_NewsUpdate] PRIMARY KEY CLUSTERED 
(
	[NewsUpdateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaskMaster]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskMaster](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[FromUserId] [int] NOT NULL,
	[SentToUserId] [int] NOT NULL,
	[IsDownload] [bit] NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[CreatedByUserId] [int] NOT NULL,
 CONSTRAINT [PK_FileTxnMaster] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[VClient]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VClient]
AS
SELECT        dbo.ClientMaster.*, dbo.LoginDetail.UserName, dbo.LoginDetail.Password, dbo.LoginDetail.UserType
FROM            dbo.ClientMaster INNER JOIN
                         dbo.LoginDetail ON dbo.ClientMaster.UserId = dbo.LoginDetail.UserId

GO
/****** Object:  View [dbo].[VEmp]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VEmp]
AS
SELECT        dbo.EmpMaster.*, dbo.LoginDetail.UserName, dbo.LoginDetail.Password, dbo.LoginDetail.UserType
FROM            dbo.EmpMaster INNER JOIN
                         dbo.LoginDetail ON dbo.EmpMaster.UserId = dbo.LoginDetail.UserId

GO
/****** Object:  View [dbo].[VTaskMasterClient]    Script Date: 17-Apr-18 10:44:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VTaskMasterClient]
AS
SELECT        dbo.TaskMaster.*, dbo.LoginDetail.UserType, dbo.ClientMaster.ClientId, dbo.ClientMaster.FullName, dbo.ClientMaster.Designation, dbo.ClientMaster.EmailId, 
                         dbo.ClientMaster.MobileNo, dbo.ClientMaster.Address, dbo.ClientMaster.UserId
FROM            dbo.TaskMaster INNER JOIN
                         dbo.ClientMaster ON dbo.TaskMaster.CreatedByUserId = dbo.ClientMaster.UserId INNER JOIN
                         dbo.LoginDetail ON dbo.ClientMaster.UserId = dbo.LoginDetail.UserId

GO
SET IDENTITY_INSERT [dbo].[ClientMaster] ON 

GO
INSERT [dbo].[ClientMaster] ([ClientId], [FullName], [Designation], [EmailId], [MobileNo], [Address], [CreatedOn], [UserId]) VALUES (1, N'rajj', N'ca', N'raj@gmail.com', N'4539479345', N'reijfoijvas', CAST(0x0000A8C10182AF82 AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[ClientMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[EmpMaster] ON 

GO
INSERT [dbo].[EmpMaster] ([EmployeeId], [EmployeeName], [Designation], [EmailId], [MobileNo], [Address], [CreatedOn], [UserId]) VALUES (1, N'pradeep lodhi', N'CA', N'pradeep@gmail.co', N'9993184672', N'bhopalvds', CAST(0x0000A8C2012C8A25 AS DateTime), 1)
GO
INSERT [dbo].[EmpMaster] ([EmployeeId], [EmployeeName], [Designation], [EmailId], [MobileNo], [Address], [CreatedOn], [UserId]) VALUES (2, N'Administrator', N'CEO', N'pradeep@loginsoftware.in', N'9993184672', N'hospital road vidisha', CAST(0x0000A8C2012C6A32 AS DateTime), 3)
GO
SET IDENTITY_INSERT [dbo].[EmpMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[LoginDetail] ON 

GO
INSERT [dbo].[LoginDetail] ([UserId], [FullName], [UserName], [Password], [UserType], [CreatedOn]) VALUES (1, N'Employer', N'emp', N'123', N'Employee', CAST(0x0000A8C2012C8A23 AS DateTime))
GO
INSERT [dbo].[LoginDetail] ([UserId], [FullName], [UserName], [Password], [UserType], [CreatedOn]) VALUES (2, N'raj', N'raj', N'raj', N'Client', CAST(0x0000A8C10182AF81 AS DateTime))
GO
INSERT [dbo].[LoginDetail] ([UserId], [FullName], [UserName], [Password], [UserType], [CreatedOn]) VALUES (3, N'Administrator', N'admin', N'admin@#123pass', N'Admin', CAST(0x0000A8C2012C6A2B AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[LoginDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[NewsUpdate] ON 

GO
INSERT [dbo].[NewsUpdate] ([NewsUpdateId], [Description], [FilePath], [CreatedOn]) VALUES (8, N'New GST updates 2018', N'ReadWrite/logo-Copy(2).png', CAST(0x0000A8C201195D92 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[NewsUpdate] OFF
GO
SET IDENTITY_INSERT [dbo].[TaskMaster] ON 

GO
INSERT [dbo].[TaskMaster] ([TaskId], [Description], [FilePath], [CreatedOn], [FromUserId], [SentToUserId], [IsDownload], [Status], [CreatedByUserId]) VALUES (1, N'Declaration', N'ReadWrite/declaration_2018.pdf', CAST(0x0000A8C500D8D232 AS DateTime), 1, 2, 0, N'Work Done & Submitted by Employee Pradeep Lodhi', 2)
GO
INSERT [dbo].[TaskMaster] ([TaskId], [Description], [FilePath], [CreatedOn], [FromUserId], [SentToUserId], [IsDownload], [Status], [CreatedByUserId]) VALUES (2, N'tax reciept work done check by admin', N'ReadWrite/Tax_3055.pdf', CAST(0x0000A8C5016E80D3 AS DateTime), 3, 2, 0, N'Work Done & Submitted by Employee Administrator', 2)
GO
INSERT [dbo].[TaskMaster] ([TaskId], [Description], [FilePath], [CreatedOn], [FromUserId], [SentToUserId], [IsDownload], [Status], [CreatedByUserId]) VALUES (3, N'test new file', N'ReadWrite/Tax_3056.pdf', CAST(0x0000A8C5017281B7 AS DateTime), 3, 1, 0, N'Assigned to Employee pradeep lodhi', 2)
GO
SET IDENTITY_INSERT [dbo].[TaskMaster] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UK_UserName]    Script Date: 17-Apr-18 10:44:54 PM ******/
ALTER TABLE [dbo].[LoginDetail] ADD  CONSTRAINT [UK_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Admin Employee Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LoginDetail', @level2type=N'COLUMN',@level2name=N'UserType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New Open Close Pending' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TaskMaster', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ClientMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoginDetail"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 194
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VClient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VClient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "EmpMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 211
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoginDetail"
            Begin Extent = 
               Top = 9
               Left = 303
               Bottom = 185
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VEmp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VEmp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TaskMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 209
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "ClientMaster"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 199
               Right = 422
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "LoginDetail"
            Begin Extent = 
               Top = 6
               Left = 460
               Bottom = 181
               Right = 630
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTaskMasterClient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VTaskMasterClient'
GO
